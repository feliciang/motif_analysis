#!/bin/bash
#===============================================================================
#
#  FILE:        filterGCpctPalindrome.sh
#
#  USAGE:       ./filterGCpctPalindrome.sh []
#
#  DESCRIPTION: 
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      10-Oct-2013
#===============================================================================

# User input variable
summarysigfile=$1
stranddir=$2

# Create bed file directory
workdir=$PWD
beddir="$workdir/bed/"
if [ ! -d "$beddir" ] ; then
	mkdir $beddir
fi
pwmdir="$workdir/pwm/"
if [ ! -d "$pwmdir" ] ; then
	mkdir $pwmdir
fi

# (1) Read "summary_sig_*.txt" file and create bed file of all motif pair incidence
perl "$REPOS/motif_analysis/PROJECT_PAIRED_MOTIFS/exp_enrichment/getBed.pl" $summarysigfile $stranddir
mv *.bed $beddir
cd $beddir

# (2) Process all *.bed files - check if motif pair PWM pass the %GC threshold (<=thr)

# Create output file for GC filtering step
cp $summarysigfile "${summarysigfile/.txt}_gcfiltered.txt"

for file in *.bed;
do
	dataset="${file/.bed}"
	
	# Convert bed files to fasta
	genome="/home/fn231/pacific/Felicia/RESOURCES/reference_genomes/mouse/mm10/ilmn_genome/ucsc/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa"
	bedtools getfasta -fi $genome -bed $file -fo "$dataset.fasta"
	
	# Get letter (ACGT) frequencies from fasta file and create .pwm files
	homerTools freq "$dataset.fasta" | grep -v "Offset" | cut -f 2-5 > "$dataset.freq"
	
	# Check if PWM pass GC threshold (<=thr).  If yes, print to file.
	status=$(Rscript --quiet "$REPOS/motif_analysis/PROJECT_PAIRED_MOTIFS/process_distance/gcfilter.R" 'dataset="'$dataset'"')
	
	if [ $status -eq 1 ]; then
		id=$(echo $dataset | awk 'BEGIN{FS="_";OFS=""}; {print $1,"_",$2,"_",$3}')
		offset=$(echo $dataset | awk 'BEGIN{FS="_"}; {print $4}')
		sed -ni.bak '/'$id'\t'$offset'/!p' "$workdir/${summarysigfile/.txt}_gcfiltered.txt"
	fi
	
	# Delete temporary files
	rm "$dataset.fasta" "$dataset.freq"
done
mv *.pwm $pwmdir
cd $workdir

# (3) Discard duplicates for palindromic motifs
outfile="${summarysigfile/.txt}_processed.txt"
Rscript --quiet "$REPOS/motif_analysis/PROJECT_PAIRED_MOTIFS/process_distance/palindromefilter.R" 'inFile="'${summarysigfile/.txt}_gcfiltered.txt'"' 'outFile="'$outfile'"'

# Delete temporary files
rm "${summarysigfile/.txt}_gcfiltered.txt.bak"