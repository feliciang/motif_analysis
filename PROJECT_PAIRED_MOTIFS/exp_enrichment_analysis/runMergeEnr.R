mpannot <- read.table("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/6_process_distance/summary_sig_1e-2_annot.txt", sep="\t", header=T)
rownames(mpannot) <- paste(mpannot[,1], mpannot[,4], sep="_")

setwd("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/exp_enrichment/")
source("/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_PAIRED_MOTIFS/sig_results/mergeEnr.R")
sigmat <- mergeEnr("sig")
qmat <- mergeEnr("q")

ssum <- apply(sigmat, 1, sum)
qmax <- apply(qmat, 1, max)
sigmat.f <- sigmat[which(ssum!=0 & qmax >=20), ]

# All samples, all motif pairs
setwd("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/2_mergedmat_1e-8_count20/")
hr <- hclust(d = dist(sigmat.f, method = "binary"), method = "complete")
hc <- hclust(d = dist(t(sigmat.f), method = "binary"), method = "complete")
sigmat.f.hc <- sigmat.f[hr$order, hc$order]
write.table(sigmat.f.hc, file="sigmat.txt", sep="\t", row.names=T, quote=F)

# Get motif names, motif url and sample url
idx <- match(rownames(sigmat.f.hc), rownames(mpannot))
motifnames <- mpannot[idx,2:3]
write.table(motifnames, file=paste("sigmat_motifnames.txt", sep=""), sep="\t", row.names=F, quote=F)

motifurl <- sapply(rownames(sigmat.f.hc), function(x) paste("=HYPERLINK(\"http://lila.results.cscr.cam.ac.uk/Felicia/motif_pair/", x, ".html\", \"", x, "\")", sep=""))
write.table(motifurl, file=paste("sigmat_motifurl.txt", sep=""), sep="\t", row.names=F, col.names=F, quote=F)

sampleurl <- colnames(sigmat.f.hc)
idx <- match(sampleurl, sortedsamples[,1])
sortedsamples2 <- sortedsamples[idx,]

idx <- grep("GSM", sortedsamples2[,7])
gsmid <- gsub(" ", "", sortedsamples2[idx,7])
idx2 <- match(sortedsamples2[idx,1], sampleurl)
gsmid2 <- cbind(sampleurl[idx2], sapply(gsmid, function(x) (unlist(strsplit(x, "-")))[1]))
sampleurl[idx2] <- apply(gsmid2, 1, function(x) paste("=HYPERLINK(\"http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=", x[2], "\", \"", x[1], "\")", sep=""))

idx <- grep("MTAB", sortedsamples2[,7])
if (length(idx)>0) {
	mtabid <- gsub(" ", "", sortedsamples2[idx,7])
	idx2 <- match(sortedsamples2[idx,1], sampleurl)
	mtabid2 <- cbind(sampleurl[idx2], sapply(mtabid, function(x) (unlist(strsplit(x, "-")))[1]))
	sampleurl[idx2] <- apply(mtabid2, 1, function(x) paste("=HYPERLINK(\"https://www.ebi.ac.uk/arrayexpress/experiments/", x[2], "/\", \"", x[1], "\")", sep=""))	
}

idx <- grep("ERX", sortedsamples2[,7])
if (length(idx)>0) {
	erxid <- gsub(" ", "", sortedsamples2[idx,7])
	idx2 <- match(sortedsamples2[idx,1], sampleurl)
	erxid2 <- cbind(sampleurl[idx2], sapply(erxid, function(x) (unlist(strsplit(x, "-")))[1]))
	sampleurl[idx2] <- apply(erxid2, 1, function(x) paste("=HYPERLINK(\"http://sra.dnanexus.com/experiments/", x[2], "/samples\", \"", x[1], "\")", sep=""))	
}

write.table(sampleurl, file=paste("sigmat_sampleurl.txt", sep=""), sep="\t", row.names=F, col.names=F, quote=F)

# Sort matrix by sample type
sortedsamples <- read.table("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/samples_annot/HAEMCODE_samples_sort_131107.txt", sep="\t", header=F)
for (i in 1:length(unique(sortedsamples[,3]))) {
	idx <- which(colnames(sigmat.f) %in% sortedsamples[which(sortedsamples[,3]==i),1])
	assign(paste("sigmat.f", i, sep=""), sigmat.f[,idx])
}

# Write individual matrix to file
for (j in 1:length(unique(sortedsamples[,3]))) {
	
	mat <- get(paste("sigmat.f", j, sep=""))
	s <- apply(mat, 1, sum)
	mat.f <- mat[which(s!=0),]
	
	hr.tmp <- hclust(d = dist(mat.f, method = "binary"), method = "complete")
	hc.tmp <- hclust(d = dist(t(mat.f), method = "binary"), method = "complete")
	assign(paste("hr", j, sep=""), hr.tmp)
	assign(paste("hc", j, sep=""), hc.tmp)
	assign(paste("sigmat.f", j, ".hc", sep=""), mat.f[hr.tmp$order, hc.tmp$order])
	
	out <- get(paste("sigmat.f", j, ".hc", sep=""))
	write.table(out, file=paste("sigmat", j, ".txt", sep=""), sep="\t", row.names=T, quote=F)
	
}

# Generate URL - correct for main matrix
# http://lila.results.cscr.cam.ac.uk/Felicia/motif_pair/MA0002.1_CN0002.1_00_11.html
# http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM898200
# https://www.ebi.ac.uk/arrayexpress/experiments/E-MTAB-1504/
# http://sra.dnanexus.com/experiments/ERX002124/samples
for (k in 1:length(unique(sortedsamples[,3]))) {
	mat <- get(paste("sigmat.f", k, ".hc", sep=""))
	#rn <- rownames(mat)
	
	# URL for motif pair
	motifurl <- sapply(rownames(mat), function(x) paste("=HYPERLINK(\"http://lila.results.cscr.cam.ac.uk/Felicia/motif_pair/", x, ".html\", \"", x, "\")", sep=""))
	write.table(motifurl, file=paste("sigmat", k, "_motifurl.txt", sep=""), sep="\t", row.names=F, col.names=F, quote=F)
	
	# URL for samples
	sampleurl <- colnames(mat)
	idx <- match(sampleurl, sortedsamples[,1])
	sortedsamples2 <- sortedsamples[idx,]
	
	idx <- grep("GSM", sortedsamples2[,7])
	gsmid <- gsub(" ", "", sortedsamples2[idx,7])
	idx2 <- match(sortedsamples2[idx,1], sampleurl)
	gsmid2 <- cbind(sampleurl[idx2], sapply(gsmid, function(x) (unlist(strsplit(x, "-")))[1]))
	sampleurl[idx2] <- apply(gsmid2, 1, function(x) paste("=HYPERLINK(\"http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=", x[2], "\", \"", x[1], "\")", sep=""))
	
	idx <- grep("MTAB", sortedsamples2[,7])
	if (length(idx)>0) {
		mtabid <- gsub(" ", "", sortedsamples2[idx,7])
		idx2 <- match(sortedsamples2[idx,1], sampleurl)
		mtabid2 <- cbind(sampleurl[idx2], sapply(mtabid, function(x) (unlist(strsplit(x, "-")))[1]))
		sampleurl[idx2] <- apply(mtabid2, 1, function(x) paste("=HYPERLINK(\"https://www.ebi.ac.uk/arrayexpress/experiments/", x[2], "/\", \"", x[1], "\")", sep=""))	
	}
	
	idx <- grep("ERX", sortedsamples2[,7])
	if (length(idx)>0) {
		erxid <- gsub(" ", "", sortedsamples2[idx,7])
		idx2 <- match(sortedsamples2[idx,1], sampleurl)
		erxid2 <- cbind(sampleurl[idx2], sapply(erxid, function(x) (unlist(strsplit(x, "-")))[1]))
		sampleurl[idx2] <- apply(erxid2, 1, function(x) paste("=HYPERLINK(\"http://sra.dnanexus.com/experiments/", x[2], "/samples\", \"", x[1], "\")", sep=""))	
	}
	
	write.table(sampleurl, file=paste("sigmat", k, "_sampleurl.txt", sep=""), sep="\t", row.names=F, col.names=F, quote=F)
	
}

for (m in 1:length(unique(sortedsamples[,3]))) {
	mat <- get(paste("sigmat.f", m, ".hc", sep=""))
	idx <- match(rownames(mat), rownames(mpannot))
	motifnames <- mpannot[idx,2:3]
	write.table(motifnames, file=paste("sigmat", m, "_motifnames.txt", sep=""), sep="\t", row.names=F, quote=F)
}

#prog <- read.table("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/cell_type_specific/progenitors.txt", sep="\t", header=F)
#prog.idx <- which(as.character(colnames(sigmat.f)) %in% as.character(prog[,1]))
#sigmat.prog <- sigmat.f[,prog.idx]
#s <- apply(sigmat.prog, 1, sum)
#sigmat.prog[which(s==12),]