#!/bin/bash

destDir="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome_expEnrichment/"
samples="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/summary_sig_1e-2_gcfiltered_samples.txt"

while read LINE
do
	mv "${LINE}"* $destDir
done <$samples