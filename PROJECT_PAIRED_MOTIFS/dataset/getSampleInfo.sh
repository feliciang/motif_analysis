#!/bin/bash
#===============================================================================
#
#  FILE:        getSampleInfo.sh
#
#  USAGE:       ./getSampleInfo.sh [path_to_bed_files]
#
#  DESCRIPTION: Script to obtain sample annotations from the 'codex' database on
#				superhanz.
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      6-Jan-2014
#===============================================================================

inputtype=$1
inputFile=$2
outFile="${inputFile/.txt}_annot.txt"

while read inputline;
do
	echo $inputline
	if [ $inputtype = "filename" ]; then
		sqlstatement="\"select id, factor, GSE, GSM, filename from codex where filename='$inputline';\""
		#output=$(ssh -n lila@superhanz.cscr.cam.ac.uk "mysql -B -Dbioinformatics -u fn231 -pG0tt3ns -e $sqlstatement" | grep -v "factor")
		ssh -n lila@superhanz.cscr.cam.ac.uk "mysql -B -Dbioinformatics -u fn231 -pG0tt3ns -e $sqlstatement" | grep -v "factor" >> $outFile
	elif [ $inputtype = "GSM" ]; then
		sqlstatement="\"select id, factor, GSE, GSM, filename from codex where GSM='$inputline';\""
		ssh -n lila@superhanz.cscr.cam.ac.uk "mysql -B -Dbioinformatics -u fn231 -pG0tt3ns -e $sqlstatement" | grep -v "factor" >> $outFile
	elif [ $inputtype = "GSE" ]; then
		sqlstatement="\"select id, factor, GSE, GSM, filename, repository, status from codex where GSE='$inputline';\""
		ssh -n lila@superhanz.cscr.cam.ac.uk "mysql -B -Dbioinformatics -u fn231 -pG0tt3ns -e $sqlstatement" | grep -v "factor" >> $outFile
	elif [ $inputtype = "id" ]; then
		sqlstatement="\"select id, CT_General, CT_Subtype, factor, GSE, GSM, filename from codex where id='$inputline';\""
		ssh -n lila@superhanz.cscr.cam.ac.uk "mysql -B -Dbioinformatics -u fn231 -pG0tt3ns -e $sqlstatement" | grep -v "factor" >> $outFile
	fi
	
	#printf "%s" $output >> $outFile
	#printf "\n" >> $outFile
	
done <$inputFile