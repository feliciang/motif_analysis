#!/bin/bash
#===============================================================================
#
#  FILE:        mergeAllMotifPairCoord.sh
#
#  USAGE:       ./mergeAllMotifPairCoord.sh
#
#  DESCRIPTION: Script to merge obtain chr, start and end for all discovered 
#				motif pairs in the ./distance_strand/ folder (significant and 
#				non-significant). The coordinates are then sorted and merged 
#				into a single file.   
#
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      8-May-2014
#===============================================================================

dist2bedscript="/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/dist2bed.sh"

parallel "$dist2bedscript {} mp.bed" ::: C*.dist.bed MF*.dist.bed MA00*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: MA01*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: MA0020*.dist.bed MA021*.dist.bed MA022*.dist.bed MA023*.dist.bed MA024*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: MA025*.dist.bed MA026*.dist.bed MA027*.dist.bed MA028*.dist.bed MA029*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: MA03*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: MA04*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: PF01*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: POL*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: PF000*.dist.bed PF001*.dist.bed PF002*.dist.bed PF003*.dist.bed
parallel "$dist2bedscript {} mp.bed" ::: PF004*.dist.bed PF005*.dist.bed PF006*.dist.bed PF007*.dist.bed PF008*.dist.bed PF009*.dist.bed

cat C*.mp.bed MF*.mp.bed MA00*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord.bed
cat MA01*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord2.bed
cat MA0020*.mp.bed MA021*.mp.bed MA022*.mp.bed MA023*.mp.bed MA024*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord3.bed
cat MA025*.mp.bed MA026*.mp.bed MA027*.mp.bed MA028*.mp.bed MA029*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord4.bed
cat MA03*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord5.bed
cat MA04*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord6.bed
cat PF01*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord7.bed
cat POL*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord8.bed
cat PF000*.mp.bed PF001*.mp.bed PF002*.mp.bed PF003*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord9.bed
cat PF004*.mp.bed PF005*.mp.bed PF006*.mp.bed PF007*.mp.bed PF008*.mp.bed PF009*.mp.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > coord10.bed 
cat coord*.bed | sort -k1,1 -k2,2n | mergeBed -i stdin > merged_motif_pair.bed

# Delete temporary files
rm coord*.bed