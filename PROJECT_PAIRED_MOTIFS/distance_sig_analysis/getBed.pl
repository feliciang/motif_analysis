#!/usr/bin/perl
#===============================================================================
#
#  FILE:        getBed.pl
#
#  USAGE:       ./getBed.pl [file name]
#
#  DESCRIPTION: Script to obtain motif pair coordinates (.bed file). 'start'
#				and 'end' refer to edge to edge coordinates of the motif pair.  
#
#  OPTIONS:     [file name] Name of summary file from Binomial/Poisson signifi-
#							cance test
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      13-Mar-2013 12:34:08 GMT
#===============================================================================

use strict;
use warnings;
use Bio::SeqIO;

my $file = $ARGV[0]; # Paired TF significance test (binomial/poisson) results summary file
my @data = get_file_data($file);
shift @data; # remove header

my $strandDir = $ARGV[1]; # Path to "../distance_by_strand/"
my $pwd = `pwd`;

foreach my $line (@data) {
	
	my ($pairId, $offset, $count, $pval, $adjPval, $totalct) = split ("\t", $line);

	my $distFile = $strandDir . $pairId . ".dist.bed";
	my @distData = get_file_data($distFile);
	
	# Get start and end coordinates for motif pair
	my $coordFile = $pairId . "_" . $offset . ".bed";
	if ($offset gt 0) {	
		my $cmd1 = "awk '\$10==$offset {print \$0}' $distFile | cut -f 1,2,7 > $coordFile";
		system($cmd1);
	}elsif ($offset lt 0) {
		my $cmd1 = "awk '\$10==$offset {print \$0}' $distFile | cut -f 3,5,6 | awk '{print \$2\"\t\"\$3\"\t\"\$1}' > $coordFile";
		system($cmd1);
	}
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}