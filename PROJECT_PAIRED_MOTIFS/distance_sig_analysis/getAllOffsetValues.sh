#!/bin/bash
#===============================================================================
#
#  FILE:        getAllOffsetValues.sh
#
#  USAGE:       ./getAllOffsetValues.sh
#
#  DESCRIPTION: Script to merge obtain chr, start and end for all discovered 
#				motif pairs in the ./distance_strand/ folder (significant and 
#				non-significant). The coordinates are then sorted and merged 
#				into a single file.   
#
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      8-May-2014
#===============================================================================

parallel "cut -f10 {} > {/.}.offset.txt" ::: C*.dist.bed MF*.dist.bed MA00*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: MA01*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: MA020*.dist.bed MA021*.dist.bed MA022*.dist.bed MA023*.dist.bed MA024*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: MA025*.dist.bed MA026*.dist.bed MA027*.dist.bed MA028*.dist.bed MA029*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: MA03*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: MA04*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: PF01*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: POL*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: PF000*.dist.bed PF001*.dist.bed PF002*.dist.bed PF003*.dist.bed
parallel "cut -f10 {} > {/.}.offset.txt" ::: PF004*.dist.bed PF005*.dist.bed PF006*.dist.bed PF007*.dist.bed PF008*.dist.bed PF009*.dist.bed

cat C*.offset.txt MF*.offset.txt MA00*.offset.txt | sort -n | uniq -c > alloffset1.txt
cat MA01*.offset.txt | sort -n | uniq -c > alloffset2.txt
cat MA020*.offset.txt MA021*.offset.txt MA022*.offset.txt MA023*.offset.txt MA024*.offset.txt | sort -n | uniq -c > alloffset3.txt
cat MA025*.offset.txt MA026*.offset.txt MA027*.offset.txt MA028*.offset.txt MA029*.offset.txt | sort -n | uniq -c > alloffset4.txt
cat MA03*.offset.txt | sort -n | uniq -c > alloffset5.txt
cat MA04*.offset.txt | sort -n | uniq -c > alloffset6.txt
cat PF01*.offset.txt | sort -n | uniq -c > alloffset7.txt
cat POL*.offset.txt | sort -n | uniq -c > alloffset8.txt
cat PF000*.offset.txt PF001*.offset.txt PF002*.offset.txt PF003*.offset.txt | sort -n | uniq -c > alloffset9.txt
cat PF004*.offset.txt PF005*.offset.txt PF006*.offset.txt PF007*.offset.txt PF008*.offset.txt PF009*.offset.txt | sort -n | uniq -c > alloffset10.txt
paste alloffset*.txt > offset_distribution.txt

# Delete temporary files
rm *.offset.txt alloffset*.txt