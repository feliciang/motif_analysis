#!/bin/bash
#<html>
#<body>
# 
#<h3>Motif Logo</h3>
#<img border="0" src="./logos2/MA0002.1_CN0002.1_00_11.png" alt="name" />
# 
#<h3>Enrichment</h3>
#<img border="0" src="MA0002.1_CN0002.1_00_11.hypTest.png" width=960 alt="name" />
#
#</body>
#</html>

samples="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/html/motif_pair_ids.txt"

while read LINE
do
	htmlfile="${LINE}.html"
	echo -e "<html>" > $htmlfile
	echo -e "<body>\n" >> $htmlfile
	echo -e "<h3>Motif Logo</h3>" >> $htmlfile
	echo -e "<img border=\"0\" src=\"./logo/${LINE}.png\" alt=\"motif logo\" />\n" >> $htmlfile
	echo -e "<h3>Enrichment barchart</h3>" >> $htmlfile
	echo -e "<img border=\"0\" src=\"./barchart/${LINE}.hypTest.png\" width=960 alt=\"enrichment barchart\" />\n" >> $htmlfile
	echo -e "<body>" >> $htmlfile
	echo -e "<html>" >> $htmlfile
done <$samples