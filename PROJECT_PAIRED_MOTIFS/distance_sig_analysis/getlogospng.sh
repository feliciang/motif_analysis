#!/bin/bash

destLogoDir="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/html/logo/"
destBarDir="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/html/barchart/"
samples="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/html/motif_pair_ids.txt"

while read LINE
do
	logoFile="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome/logos2/${LINE}.png"
	htFile="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/filterGCpctPalindrome_expEnrichment/${LINE}.hypTest.png"
	mv $logoFile $destLogoDir
	mv $htFile $destBarDir
done <$samples