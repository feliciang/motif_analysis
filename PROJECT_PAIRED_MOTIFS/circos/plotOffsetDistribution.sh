#!/bin/bash

bedDir="/home/fn231/pacific/Felicia/ANALYSIS_2/1_motif_pair/6_process_distance/bed/"
sigMotifPairCl="/home/fn231/pacific/Felicia/ANALYSIS_2/1_motif_pair/7_analysis/1_significant_motif_pairs_qval1e-4/circos/summary_sig_qval-lte1e-4_annot_cl.txt"

# Merge coordinates for motif pairs in the same group (e.g. Ets + Homeobox)
while read pairgrp; do

	motif1cl=$(echo $pairgrp | cut -d"_" -f1)
	motif2cl=$(echo $pairgrp | cut -d"_" -f2)
	#echo $pairgrp
	#echo $motif1cl
	#echo $motif2cl
	
	grep "$motif1cl$motif2cl" $sigMotifPairCl > subset.txt
	grep "$motif2cl$motif1cl" $sigMotifPairCl >> subset.txt
	# echo uniq

	grep -v "motif_pair_id" subset.txt | awk -v bd=$bedDir 'BEGIN{FS="\t"; OFS=""} {print bd,$1,"_",$4,".bed"}' > bedFileNames.txt
	
	cat bedFileNames.txt | xargs cat | sort -n -k1b,1 -k2,2 | uniq >> all_coord.txt 

	# Remove temporary files
	#rm subset.txt bedFileNames.txt
		
done<pair_list.txt