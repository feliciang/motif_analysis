#!/usr/bin/perl
#===============================================================================
#
#  FILE:         summarizeSigResults.pl
#
#  USAGE:        ./summarizeSigResults.pl [file name] [pvalthr]
#
#  DESCRIPTION:  Script to summarize significant results from processDistanceFile.pl
#				 (concatenate all offset values for the same motif pair ids)
#
#  OPTIONS:      [file name] Name of summary file from Binomial/Poisson signifi-
#							 cance test
#				 [pvalthr] p-value threshold for filtering 'adj.pval' column
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      11-Jul-2013 16:20:19 GMT
#===============================================================================

use strict;
use warnings;

my ($file, $pvalthr) = @ARGV; # Paired TF significance test (binomial/poisson) results summary file; pval threshold
my @data = get_file_data($file);
shift @data; # remove header

# Initialize arrays
my @offset;
my @count;

# Output file
$file =~ m/([A-Za-z0-9._+-]+)(.txt)/;
my $fileprefix = $1;
my $outFile = $fileprefix . "_merged_adjp" . $pvalthr . ".txt";
open( OUT, ">$outFile" ) or die "Can't open output file !!";
print OUT "motif_pair_id\toffset\tcount\ttotalpairs\n";

# Read line 1
my $line1 = shift @data;
my ($refPairId, $refOffset, $refCount, $refPval, $refAdjPval, $refTotalct) = split ("\t", $line1);
push(@offset, $refOffset);
push(@count, $refCount);

# Read rest of file
foreach my $line (@data) {
	
	my ($pairId, $offset, $count, $pval, $adjPval, $totalct) = split ("\t", $line);

	if ($adjPval <= $pvalthr) {
		# Check if ID is same as line before
		if ($pairId eq $refPairId) {
			push(@offset, $offset);
			push(@count, $count);
		}else {
			# Print merged results
			if (scalar(@offset)>1) {
				my @order = sort { $offset[$a] <=> $offset[$b] } 0 .. $#offset;
				@offset = @offset[@order];
				@count = @count[@order];	
			}			
			chomp($refTotalct);
			print OUT $refPairId, "\t", join(";", @offset), "\t", join(";", @count), "\t", $refTotalct, "\n";
			# Re-initialize $ref*
			($refPairId, $refOffset, $refCount, $refPval, $refAdjPval, $refTotalct) = ($pairId, $offset, $count, $pval, $adjPval, $totalct);
			undef @offset;
			undef @count;
			push(@offset, $refOffset);
			push(@count, $refCount);
		}	
	}else {
		next;
	}
	
}

# Print merged results
my @order = sort { $offset[$a] <=> $offset[$b] } 0 .. $#offset;
@offset = @offset[@order];
@count = @count[@order];
chomp($refTotalct);
print OUT $refPairId, "\t", join(";", @offset), "\t", join(";", @count), "\t", $refTotalct, "\n";
			
sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

sub trim {

	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	chomp $string;
	return $string;

}