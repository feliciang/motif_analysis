#!/usr/bin/perl
#===============================================================================
#
#  FILE:        processDistanceFile.pl
#
#  USAGE:       ./processDistanceFile.pl [path]
#
#  DESCRIPTION: Script to process all distance (*.dist.bed) files
#
#  OPTIONS:     [path] path to distance files
# 
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      05-Mar-2013 15:34:08 GMT
#===============================================================================

use strict;
use warnings;
use Switch;

my $dir = $ARGV[0];
opendir DIR1, $dir or die "Couldn't open paired TF distance results dir '$dir': $!";
my @dirlist1 = readdir(DIR1);
closedir DIR1;

my $outdir = "$ENV{'WORKDIR'}/ANALYSIS_2/1_motif_pair/";
my $distsigdir = $outdir . "6_process_distance/distance_sig/";
my $stranddir = $outdir . "6_process_distance/distance_strand/";
my $summaryDir = $outdir . "6_process_distance/";
my $numTestFiles = 0;
my $colNum = 10;
my $pthr = 0.01;

my $logFile = $outdir . "processDistanceFile.log";
open( LOG, ">$logFile" ) or die "Can't open log file !!";

# Iterate over all results directory (1 for every peak file)
foreach my $distFile (@dirlist1) {
	
	# Ignore files beginning with a period
	next if ($distFile =~ m/^\./);
	# Ignore non distance files
	next if ($distFile !~ m/.dist.bed$/);
	
	my @distData = get_file_data($distFile);
	
#	# Ignore files with fewer than 50 co-occurences of paired TFs
#	next if ( (scalar @distData) < 50 );
	
	print LOG "Processing file: $distFile\n";
	$distFile =~ m/([A-Za-z0-9._]+)(.dist.bed)/;
	my $id = $1;
	
	# Split files 4 ways based on strand: ++, --, +-, -+	
	for (my $i=1; $i<=4; $i++) {
		
		my $str;
		my $inputFile;
		
		switch ( $i ) {
		    case 1 { 	# ++ strand
		    	$str = $id . "_00";
		    	my $distFile1 = $str . ".dist.bed";
		    	my $cmd1 = "awk '{ if (\$4 == \"+\" && \$8 == \"+\") {print \$0} }' $distFile > $distFile1";
				system($cmd1);
		    	$inputFile = $distFile1;
		    }
		    case 2 { 	# -- strand
		    	$str = $id . "_11"; 
		    	my $distFile2 = $str . ".dist.bed";
				my $cmd2 = "awk '{ if (\$4 == \"-\" && \$8 == \"-\") {print \$0} }' $distFile > $distFile2";
				system($cmd2);
				$inputFile = $distFile2;
		    }
		    case 3 { 	# +- strand
		    	$str = $id . "_01"; 
				my $distFile3 = $str . ".dist.bed";
				my $cmd3 = "awk '{ if (\$4 == \"+\" && \$8 == \"-\") {print \$0} }' $distFile > $distFile3";
				system($cmd3);	    	
		    	$inputFile = $distFile3; 
		    }
		    case 4 {	# -+ strand
		    	$str = $id . "_10";
				my $distFile4 = $str . ".dist.bed";
				my $cmd4 = "awk '{ if (\$4 == \"-\" && \$8 == \"+\") {print \$0} }' $distFile > $distFile4";
				system($cmd4);	    	
		    	$inputFile = $distFile4;
		    }
		}
		
		my $wcout = `wc $inputFile`;
		my @wc = split (" ", $wcout);
		my $filelength = $wc[0];

		# Check that dist-strand file is not empty
		if (-s $inputFile) {
			if ($filelength <= 2000) {
				# Run Binomial test : R script to plot histogram and calculate p-value
				my $binomFile = $distsigdir . $str . ".sig.txt";
				my $histFile = $distsigdir . $str . ".hist.pdf";
				my $script = "$ENV{'REPOS'}/motif_analysis/PROJECT_PAIRED_MOTIFS/distance/computeBinomialTest.R";
				my $cmd5 = "Rscript --quiet $script 'inputFile=\"$inputFile\"' 'colNum=$colNum' 'outputFile=\"$binomFile\"' 'histFile=\"$histFile\"'";
				system($cmd5);
				next;
			}elsif ($filelength > 2000) {
				# Run Poisson test : R script to plot histogram and calculate p-value
				my $binomFile = $distsigdir . $str . ".sig.txt";
				my $histFile = $distsigdir . $str . ".hist.pdf";
				my $script = "$ENV{'REPOS'}/motif_analysis/PROJECT_PAIRED_MOTIFS/distance/computePoissonTest.R";
				my $cmd5 = "Rscript --quiet $script 'inputFile=\"$inputFile\"' 'colNum=$colNum' 'outputFile=\"$binomFile\"' 'histFile=\"$histFile\"'";
				system($cmd5);
			}
		}elsif (!-s $inputFile){
			system("rm $distFile");	# remove empty file
		}

	}
	
	# Move strand-specific files to another folder
	my $cmd7 = "mv $id\_\*\.dist.bed $stranddir";
	system($cmd7);
	
	$numTestFiles++;
	
}

print LOG "Total files tested: $numTestFiles\n";

# Go to results folder and summarize results
chdir($distsigdir) or die "Can't chdir to $distsigdir $!";
opendir DIR2, $distsigdir or die "Couldn't open significance test results dir '$distsigdir': $!";
my @dirlist2 = readdir(DIR2);
closedir DIR2;

my %sigresults = ();
my %totalpair = ();

# Iterate over all *.sig.txt files and write significant results to a file
foreach my $binomFile (@dirlist2) {
	
	# Ignore files beginning with a period
	next if ($binomFile =~ m/^\./);
	# Ignore non distance files
	next if ($binomFile !~ m/.sig.txt$/);
	
	my @binomData = get_file_data($binomFile);
	shift @binomData;
	$binomFile =~ m/([A-Za-z0-9._+-]+)(.sig.txt)/;
	my $motifpairid = $1;
	my $totalcount = 0;
	
	foreach my $line (@binomData) {
		(my $pos, my $count, my $pval, my $adjpval) = split ( "\t", $line );
		$totalcount += $count;
		if ($adjpval<=$pthr) {
			# Bonferroni correction for number of paired TFs tested and 4 combinations of strand orientation
			my $adjpval2 = $adjpval*$numTestFiles*4;
			if ($adjpval2<=$pthr) {
				$sigresults{$motifpairid}{$pos}{'count'} = $count;
				$sigresults{$motifpairid}{$pos}{'pval'} = $pval;
				$sigresults{$motifpairid}{$pos}{'adjpval'} = $adjpval2;
			}
		}
	}
	$totalpair{$motifpairid} = $totalcount;

}

# Print results file
my $sci_pthr = sprintf("%e", $pthr);
my $sigFile = $summaryDir . "summary_sig_" . $sci_pthr . ".txt";
open( OUT, ">$sigFile" ) or die "Can't open output file: $sigFile !!";
print OUT "motif_pair_id\toffset\tcount\tpval\tadj.pval\ttotalpairs\n";

for my $motifpairid ( keys %sigresults ) {
	for my $pos ( keys %{$sigresults{$motifpairid}} ) {
		print OUT $motifpairid, "\t", $pos, "\t", $sigresults{$motifpairid}{$pos}{'count'}, "\t";
		print OUT $sigresults{$motifpairid}{$pos}{'pval'}, "\t", $sigresults{$motifpairid}{$pos}{'adjpval'}, "\t";
		print OUT $totalpair{$motifpairid}, "\n";
	}
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
    
}