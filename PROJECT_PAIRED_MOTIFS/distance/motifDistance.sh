#!/bin/bash
#===============================================================================
#
#  FILE:        motifDistance.sh
#
#  USAGE:       ./motifDistance.sh [path_to_bed_files]
#
#  DESCRIPTION: Compare all against all motif locations (bed format). Reports
#				distance between any two motifs. Constraints??
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      01-Mar-2013
#===============================================================================

currdir=$PWD
processedDir="$WORKDIR/ANALYSIS_2/1_motif_pair/5_distance/processed"
outputDir="$WORKDIR/ANALYSIS_2/1_motif_pair/5_distance/distance"
minInstance=50

# Process all bed files
for file in *.bed;
do
	n=($(wc $file)) # Number of motif instances
	
	if [ ${n[0]} -ge $minInstance ]; then
		echo "Processing file:  $file"
		# Sort bed files and keep unique coordinates
		sort -n -k 1b,1 -k 2,2 $file | uniq > "$processedDir/$file"
		
		# Check that each processed bed file has no overlapping features 
		subtractBed -a "$processedDir/$file" -b "$processedDir/$file" > subtractBed.out
		if [ -s subtractBed.out ]; then
			echo "\tWARNING: $file has overlapping features!"
		fi
		rm subtractBed.out
	fi
	
done

cd $processedDir
bedFiles=(`ls *.bed`)

# Iterate over all bed files
for file1 in "${bedFiles[@]}";
do
	echo "Calculating distance for file:  $file1"
	for file2 in "${bedFiles[@]}";
	do
		if [ "$file2" == "$file1" ]; then
			break
		else
			# Get all features within 100bp either side and calculate the distance
			outputFile="${file1/.bed}_${file2/.bed}.dist.bed"
			wbedFile="${file1/.bed}_${file2/.bed}.wbed.txt"
			windowBed -a $file1 -b $file2 -w 101 | getOverlap -i stdin -cols 2,3,6,7 > windowBedOut.txt
			if [ -s windowBedOut.txt ]; then
				# Process offset value. Only positive and negative values are valid. Rows with zero offset value are ignored.
				awk 'BEGIN{FS="\t";OFS="\t";} { if($6>$3) {print $0,($6-$3);} else if ($2>$7) {print $0,($7-$2);} }' windowBedOut.txt > "$outputDir/$outputFile" 
				rm windowBedOut.txt
				nlines=`wc "$outputDir/$outputFile" | awk 'BEGIN{FS=" "} {print $1}'`
				if [ ${nlines} -lt 50 ]; then	# Discard file if there are less than 50 occurrences of motif pairs
					rm "$outputDir/$outputFile"
				fi
			else
				rm windowBedOut.txt
			fi
		fi
	done
done

cd $currdir