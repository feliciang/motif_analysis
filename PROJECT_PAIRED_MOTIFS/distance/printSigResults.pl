#!/usr/bin/perl
#===============================================================================
#
#  FILE:        printSigResults.pl
#
#  USAGE:       ./printSigResults.pl [path] [total files tested]
#
#  DESCRIPTION: Script get all significant results from processDistanceFile.pl
#
#  OPTIONS:     [path] path to *.sig.txt files
#				[total files tested] number indicating how many distance files
#				were tested
# 
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      05-Mar-2013 15:34:08 GMT
#===============================================================================

use strict;
use warnings;
use Switch;

my $dir = $ARGV[0];  # Results folder
opendir DIR1, $dir or die "Couldn't open paired TF distance results dir '$dir': $!";
my @dirlist1 = readdir(DIR1);
closedir DIR1;

my $numTestFiles = $ARGV[1];  # Total files tested (see last line of LOG.txt from processDistanceFile.pl)

my %sigresults = ();
my %totalpair = ();
my $pthr = 1;

# Iterate over all *.sig.txt files and write significant results to a file
foreach my $binomFile (@dirlist1) {
	
	# Ignore files beginning with a period
	next if ($binomFile =~ m/^\./);
	# Ignore non distance files
	next if ($binomFile !~ m/.sig.txt$/);
	
	my @binomData = get_file_data($binomFile);
	shift @binomData;
	$binomFile =~ m/([A-Za-z0-9._+-]+)(.sig.txt)/;
	my $motifpairid = $1;
	my $totalcount = 0;
	
	foreach my $line (@binomData) {
		(my $pos, my $count, my $pval, my $adjpval) = split ( "\t", $line );
		$totalcount += $count;
		#if ($adjpval<=$pthr) {
			# Bonferroni correction for number of paired TFs tested and 4 combinations of strand orientation
			my $adjpval2 = $adjpval*$numTestFiles*4;
			#if ($adjpval2<=$pthr) {
			if ($count>=$pthr) {
				$sigresults{$motifpairid}{$pos}{'count'} = $count;
				$sigresults{$motifpairid}{$pos}{'pval'} = $pval;
				$sigresults{$motifpairid}{$pos}{'adjpval'} = $adjpval2;
			}
		#}
	}
	$totalpair{$motifpairid} = $totalcount;

}

# Print results file
my $sci_pthr = sprintf("%e", $pthr);
my $sigFile = "summary_sig_" . $sci_pthr . ".txt";
open( OUT, ">$sigFile" ) or die "Can't open output file: $sigFile !!";
print OUT "motif_pair_id\toffset\tcount\tpval\tadj.pval\ttotalpairs\n";

for my $motifpairid ( keys %sigresults ) {
	for my $pos ( sort {$a<=>$b} keys %{$sigresults{$motifpairid}} ) {
		print OUT $motifpairid, "\t", $pos, "\t", $sigresults{$motifpairid}{$pos}{'count'}, "\t";
		print OUT $sigresults{$motifpairid}{$pos}{'pval'}, "\t", $sigresults{$motifpairid}{$pos}{'adjpval'}, "\t";
		print OUT $totalpair{$motifpairid}, "\n";
	}
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
    
}