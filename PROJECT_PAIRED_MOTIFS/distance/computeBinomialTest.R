#! /usr/bin/Rscript --vanilla
#===============================================================================
#
#  SCRIPT:	    computeBinomialTest.R
#
#  DESCRIPTION: Compute Binomial significance statistic for each motif pair
#				distance data. One-sided test for distribution of x greater than
#				null.
#				
#  AUTHOR:      Felicia Ng (fn231)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     7 Mar 2013
#===============================================================================

suppressMessages(library(ggplot2))
suppressMessages(library(reshape))

args <- (commandArgs(TRUE)) # inputFile, colNum, outputFile, histFile
n <- length(args)

if(n==0){
	print("computeKS.R error: No arguments supplied.")
}else if (n!=4) {
	print("computeKS.R error: Incorrect number of arguments supplied!")
}else if (length(args)==4) {
	for (i in 1:length(args)) {
		eval(parse(text=args[[i]]))
	}
}
distData <- read.table(as.character(inputFile), sep="\t")

# Plot histogram
pdf(histFile)
ggplot(distData, aes_string(x=colnames(distData)[colNum])) + geom_histogram(binwidth=1) + labs(x = "offset (bp)") + theme_bw()
do <- dev.off()

# Get counts for every position from -100 to 100 relative to primary motif
h <- hist(distData[,colNum], breaks=-101:100, plot=F)
expPr <- 1/(4*100) # 100 possible offset values
		
btmat <- cbind(-100:100, h$counts, rep(0,201), rep(0,201))
colnames(btmat) <- c("pos", "count", "pval", "adj.pval")
for (j in 1:201) {
	bt <- binom.test(x=h$counts[j], n=sum(h$counts), p=expPr, alternative="greater")
	btmat[j,3] <- bt$p.value
}
btmat <- btmat[-101,] # remove row where offset=0
btmat[,4] <- p.adjust(btmat[,3], method="bonferroni") # Bonferroni correction for number of sites (200) tested

write.table(btmat, file=outputFile, sep="\t", row.names=F, quote=F)