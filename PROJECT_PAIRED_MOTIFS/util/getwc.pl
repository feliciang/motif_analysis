#!/usr/bin/perl
#===============================================================================
#
#  FILE:         getwc.pl
#
#  USAGE:        ./getwc.pl [path] [pattern]
#
#  DESCRIPTION:  Counts number of lines in all files that matches $pattern.  
#
#  OPTIONS:      [path] 		path to files
#				 [pattern]	pattern		
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      28-Jun-2013 14:34:08 GMT
#===============================================================================

my ($dir, $pattern) = @ARGV;
opendir DIR, $dir or die "Couldn't open paired TF distance results dir '$dir': $!";
my @inputlist = readdir(DIR);
closedir DIR;

foreach my $inputFile (@inputlist) {
	
	# Ignore files beginning with a period
	next if ($inputFile =~ m/^\./);
	
	# Ignore files that don't match $pattern
	if ($inputFile =~ m/$pattern/g) {
		my $wcfull = `wc $inputFile`;
		my @wcelem = split (" ", $wcfull);
		print $inputFile, "\t", $wcelem[0], "\n";	
	}	
	
}

exit;