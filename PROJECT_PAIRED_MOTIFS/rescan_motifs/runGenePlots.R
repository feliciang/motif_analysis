# Get all conservation score file
currdir <- getwd()
dirlist <- list.files(patter=".bed")
source(paste(Sys.getenv("WORKDIR"), "/repos/util/RUTIL/sequencing/sequtils.R", sep=""))

# Output directory for merged matrix
outdir <- paste(Sys.getenv("WORKDIR"), "/ANALYSIS_2/1_motif_pair/7_analysis/2_scan_HAEMCODE_regions/3_conservationscore_peakdistribution/3_merged_matrix/", sep="")

for (i in 1:length(dirlist)) {
	
	# Conservation score
	consfile <- dirlist[i]
	sample <- gsub("_phylop_phastcons.bed", "", consfile)
	cons <- read.table(consfile, sep="\t", header=F)
	cons.rnames <- coord2str(cons[,1:3])
			
	# Peak distribution (merged)
	regdist.fname <- paste(Sys.getenv("WORKDIR"), "/ANALYSIS_2/1_motif_pair/7_analysis/2_scan_HAEMCODE_regions/3_conservationscore_peakdistribution/2_peak_distribution/all_merged/", sample, "_peaks_genes.txt", sep="")
	regdist <- read.table(regdist.fname, sep="\t", header=T)
	reg.rnames <- coord2str(regdist[,1:3])
	
	# Merge
	rnames <- intersect(reg.rnames, cons.rnames)
	idx1 <- match(rnames, reg.rnames)
	idx2 <- match(rnames, cons.rnames)
	mergedmat <- cbind(regdist[idx1,], cons[idx2,4:5])
	colnames(mergedmat) <- c("chr", "start", "end", "gene", "promoter",	"intragenic", "intergenic", "phylop", "phastcons")
	mergedmat.fname <- paste(outdir, sample, ".matrix", sep="")
	write.table(mergedmat, file=mergedmat.fname, sep="\t", row.names=F, quote=F)
	
	# Filter
	phylop.thr <- quantile(mergedmat[,"phylop"], probs=seq(0,1,0.05))[19]  # 90pctile value
	phastcons.thr <- quantile(mergedmat[,"phastcons"], probs=seq(0,1,0.05))[19]
	idx <- which(mergedmat[,"promoter"]==1 & mergedmat[,"phylop"]>=phylop.thr | mergedmat[,"promoter"]==1 & mergedmat[,"phastcons"]>=phastcons.thr)
	genes <- unique(mergedmat[idx,4])
	
	# Gene plots (BioGPS data)
	genePlotDir <- paste(Sys.getenv("WORKDIR"), "/ANALYSIS_2/1_motif_pair/7_analysis/2_scan_HAEMCODE_regions/3_conservationscore_peakdistribution/4_biogps_plots/", sample, "/", sep="")
	if (file.exists(genePlotDir)){
		setwd(genePlotDir)
	} else {
		dir.create(genePlotDir)
		setwd(genePlotDir)
	}
	source(paste(Sys.getenv("REPOS"), "/motif_analysis/PROJECT_PAIRED_MOTIFS/rescan_motifs/genePlots.R", sep=""))

	setwd(currdir)
	
}

#consensusmat <- read.table("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/1_filterGCpctPalindrome/2_motif_pair_sig_1e-8_count20/2_homer_find/find2bed/1_matrix/consensusmotifs_annot_conservation.matrix", sep="\t", header=T)
#colidx <- c(5:12,14:16)
#for (i in colidx) {
#	name <- colnames(consensusmat)[i]
#	idx <- which(consensusmat[,20]==1 & consensusmat[,21]>=1.6 & consensusmat[,17]==1 & consensusmat[,i]>=1)
#	genes <- unique(consensusmat[idx,4])
#	
#	path <- paste("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/1_filterGCpctPalindrome/2_motif_pair_sig_1e-8_count20/2_homer_find/find2bed/3_biogps_plots/", name, "/", sep="")
#	setwd(path)
#	source("/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_PAIRED_MOTIFS/sig_results_analysis/genePlots.R")
#}