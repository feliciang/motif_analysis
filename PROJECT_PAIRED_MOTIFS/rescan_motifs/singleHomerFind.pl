#!/usr/bin/perl
#===============================================================================
#
#  FILE:        batchhomerfind.pl
#
#  USAGE:       ./batchhomerfind.pl [path]
#
#  DESCRIPTION: Script to run HOMER -find analysis on ONE peak file against a
#				list of motifs
#
# OPTIONS:      [path] path to .motif files
#				[genome] genome to use
#				[findsize] size of region to be used in findMotifsGenome.pl
#							(default: 150)
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      5-Mar-2014 16:02:08 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;

my $dir = $ARGV[0];
chomp $dir;
opendir(DIR, $dir) or die $!;

my $genome = $ARGV[1] || "mm10";
my $findsize = $ARGV[2] || 150;

my $outDir = "$ENV{'WORKDIR'}/ANALYSIS_2/1_motif_pair/7_analysis/2_scan_HAEMCODE_regions/1_homer_find";
my $homerFile = "$ENV{'RESOURCES'}/compendium/mm10/samples_289/repeat_removed/all_peaks.homer.txt";

my $logFile = "singleHomerFind.log";
open( LOG, ">$logFile" ) or die "Can't open log file !!";

my $timestamp = localtime(time);
print LOG "[", scalar $timestamp, "] Running singleHomerFind.pl\n";
print LOG "--------------------------\n\n"; 
print LOG "outDir = ", $outDir, "\n";
print LOG "homerFile = ", $homerFile, "\n";
print LOG "genome = ", $genome, "\n";
print LOG "findsize = ", $findsize, "\n\n";
print LOG "Running HOMER find on: \n";

while (my $motifFile = readdir(DIR)) {
	
	# Ignore non .motif files (HOMER format)
	next if ($motifFile !~ m/.motif$/);
	
	print LOG $motifFile, "\n";
	
	my $motifname =~ s/.motif//;
	my $outFile = $outDir . "/" . $motifname. "find.txt";
	my $homercmd = "findMotifsGenome.pl $homerFile $genome $outDir -size $findsize -find $motifFile > $outFile";
	system $homercmd;

}

$timestamp = localtime(time);
print LOG "\n[", scalar $timestamp, "] Run complete\n";

# Remove temporary file
unlink("motifFindingParameters.txt") or warn "Could not unlink: motifFindingParameters.txt\n";