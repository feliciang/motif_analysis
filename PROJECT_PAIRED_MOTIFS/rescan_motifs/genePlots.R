library(ggplot2)

#genes <- read.table("/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/7_analysis/1_filterGCpctPalindrome/2_motif_pair_sig_1e-8_count20/2_homer_find/find2bed/prom_genes.txt", sep="\t", header=F)
if (!exists("biogps")) {
	biogps <- read.table("/home/fn231/pacific/Felicia/DATASETS_PUBLIC/microarray/GSE10246_GeneAtlas/ESET/emat_geneMax_sampleMean.txt", sep="\t", header=T, row.names=1, check.names=F)
}
if (!exists("annot")) {
	annot <- read.table("/home/fn231/pacific/Felicia/DATASETS_PUBLIC/microarray/GSE10246_GeneAtlas/biogps.MOE430-2.pdata_blood_uniq.txt", sep="\t", header=F)
}
if (exists("plotmat")) {
	rm(plotmat)
}

for (i in 1:length(genes)) {
	vec <- biogps[which(toupper(rownames(biogps))==genes[i]),]
	if (exists("plotmat")) {
		plotmat <- rbind(plotmat, vec)
	}else {
		plotmat <- vec
	}
}

for (j in 1:nrow(plotmat)) {
	vec <- t(as.matrix(plotmat[j,]))
	plotvec <- data.frame(vec, colnames(plotmat), annot[,2])
	colnames(plotvec) <- c("log2Intensity", "cell_type", "blood")
	genemean <- mean(plotvec[,1])
	a <- ggplot(plotvec, aes(x=cell_type, y=log2Intensity, fill=factor(blood))) + geom_bar(stat="identity", position="dodge") + 
				scale_fill_manual(values=c("black", "red")) + theme(axis.text.x=element_text(angle=90)) + geom_hline(yintercept=genemean)
	pdf(paste("biogps", "_", rownames(plotmat[j,]), ".pdf", sep=""), width=14)
	print(a)
	dev.off()
}