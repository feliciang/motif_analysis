#!/bin/bash

conSeqFile=$1
outDir="$WORKDIR/COMPENDIUM/pipeline_mm10/7_analysis/1_filterGCpctPalindrome/2_motif_pair_sig_1e-8_count20/2_homer_find/motifs"

grep -v "consensus_sequence" $conSeqFile > input.txt

while read LINE
do
	consensusSeq=$(echo $LINE | awk '{print $1}')
	name=$(echo $LINE | awk '{print $2}')
    motifFile="$name.motif"
    seq2profile.pl $consensusSeq 0 $name > "$outDir/$motifFile"
done <input.txt

rm input.txt