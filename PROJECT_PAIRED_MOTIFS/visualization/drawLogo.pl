#!/usr/bin/perl
#===============================================================================
#
#  FILE:         drawLogo.pl
#
#  USAGE:        ./drawLogo.pl [file name]
#
#  DESCRIPTION:  Script to draw motif logo.  Draws 50bp around primary motif.
#
#  OPTIONS:      [file name] Name of summary file from Binomial/Poisson signifi-
#							cance test
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      13-Mar-2013 12:34:08 GMT
#===============================================================================

use strict;
use warnings;
use Bio::SeqIO;
use POSIX;

my $file = $ARGV[0]; # Paired TF binomial/poisson test results summary file
my @data = get_file_data($file);
shift @data; # remove header

my $strandDir = "$ENV{WORKDIR}/ANALYSIS_2/1_motif_pair/6_process_distance/distance_strand/";
my $pwd = `pwd`;

foreach my $line (@data) {
	
	my($pairId, $offset, $count, $pval, $adjPval, $totalct) = split ("\t", $line);
	
	my $distFile = $strandDir . $pairId . ".dist.bed";
	my @distData = get_file_data($distFile);
	
	# Draw motif pair logo
	# Get coordinates for primary motif and resize primarymotif.bed to 50bp
	my $cmd2 = "awk '\$10==$offset {print \$0}' $distFile | cut -f 1,2,3 > primarymotif.bed";
	system($cmd2);
#	my $cmd3 = "perl $ENV{REPOS}/motif_analysis/PROJECT_PAIRED_MOTIFS/distance/make_bed_50bp.pl primarymotif.bed primarymotif_50bp.bed";
#	system($cmd3);
	my $absoffset = abs($offset);
	my $len;
	if ($absoffset > 15) {
		$len = 50 + ceil(($absoffset-15)/5)*10;
	}else {
		$len = 50;
	}	
	
	my $cmd3 = "perl $ENV{REPOS}/motif_analysis/PROJECT_PAIRED_MOTIFS/distance/make_bed.pl primarymotif.bed primarymotif_extended.bed $len";
	system($cmd3);
	
	# Extract fasta sequence
	my $cmd4 = "perl $ENV{REPOS}/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/bed2homer.pl primarymotif_extended.bed primarymotif_extended.homer.txt";
	system($cmd4);
	my $cmd5 = "homerTools extract primarymotif_extended.homer.txt /home/Programs/HOMER/data/genomes/mm10/ -fa > seq.fasta";
	system($cmd5);
#	my $genome="$ENV{RESOURCES}/reference_genomes/mouse/mm10/ilmn_genome/ucsc/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa";
#	my $cmd4 = "bedtools getfasta -fi $genome -bed primarymotif_50bp.bed -fo seq_50bp.fasta";

	# weblogo
	my $pngFile = $pairId . "_" . $offset . ".png";
	my $cmd6 = "/home/Programs/weblogo-3.3/weblogo --format png -n 100 < seq.fasta > $pngFile";
	system($cmd6);
	
	# Delete tmp files
	system("rm primarymotif.bed");
	system("rm primarymotif_50bp.bed");
	system("rm primarymotif_extended.bed");
	system("rm primarymotif_extended.homer.txt");	
	system("rm seq.fasta");
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}