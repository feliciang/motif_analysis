##!/usr/bin/perl
#===============================================================================
#
#  FILE:         checkFastaLen.pl
#
#  USAGE:        ./checkFastaLen.pl [in file name] [out file name]
#
#  DESCRIPTION:  Reads in fasta file and writes to output file if fasta length
#				 is more than 50.
#
#  OPTIONS:      [in file name] input fasta file name
#				 [out file name] output fasta file name
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      13-Apr-2013 12:40:00 GMT
#===============================================================================

use Bio::SeqIO;

my ($inFile, $outFile) = @ARGV;

my $seqIN = Bio::SeqIO->new('-format' => 'fasta', '-file' =>  "$inFile");
my $seqOUT = Bio::SeqIO->new('-format' => 'fasta', '-file' =>  ">$outFile");

while(my $seq = $seqIN->next_seq()) {
  my $len = $seq->length();
  print "length = ", $len, "\n";
  if ($len >= 50) {
  	print "writing to file!\n";
	$seqOUT->write_seq($seq);
  }
}