#!/bin/bash

# Reads in all motif pair bed files
motifDirList="*.bed"

peakDir=${1:-"$WORKDIR/ANALYSIS_2/1_motif_pair/1_repeat_masker/repeat_removed/"}

allPeaksBed=${2:-"$RESOURCES/compendium/mm10/samples_289/repeat_removed_100bp/resize_150bp/all_peaks_merged.bed"} # preferably repeat regions removed
allPeaksLen=$(wc -l $allPeaksBed | awk '{print $1}')

pval=0.0001
numTestFiles=$(ls $peakDir*.bed | wc -l | awk '{print $1}')

# Remove any old log files
if [ -f LOG.txt ]; then
	rm LOG.txt
fi

# peak file names
ls $peakDir*.bed | awk -F/ '{print $NF}' | sed 's/.bed//g' | sort > peakid.txt

# k = number of balls drawn from the urn
parallel "intersectBed -a {} -b $allPeaksBed -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq > {/.}.kOut" ::: *.bed 
parallel "wc -l {}" ::: *.kOut > kvalue.txt
rm *.kOut

#Run analysis on all significant motif pairs
for motifFile in $motifDirList; do
	
	echo "Testing: ${motifFile/.bed}" >> LOG.txt

	# m = number of white balls in the urn
	parallel "intersectBed -a {} -b $allPeaksBed -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq > {/.}.mOut" ::: $peakDir*.bed
	parallel "wc -l {}" ::: *.mOut | sort -k2,2 | awk '{print $1}' > mvalue.txt
	
	# q = number of white balls drawn without replacement
	parallel "intersectBed -a $motifFile -b {} -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq > {/.}.qOut" ::: *.mOut
	parallel "wc -l {}" ::: *.qOut | sort -k2,2 | awk '{print $1}' > qvalue.txt
	
	# n = number of black balls in the urn
	awk '{print len-$1}' len=$allPeaksLen mvalue.txt > nvalue.txt	
	
	# Write to output file
	hypFile="${motifFile/.bed}.hypTest.txt";
	kvalue=$(grep ${motifFile/.bed} kvalue.txt | awk '{print $1}') # kvalue to be appended to end of each line
	paste peakid.txt qvalue.txt mvalue.txt nvalue.txt | awk '{OFS="\t"; print $0,k}' k=$kvalue | sed "1iexperiment\tq\tm\tn\tk" > $hypFile
	
	rm *.mOut *.qOut qvalue.txt mvalue.txt nvalue.txt

done

rm peakid.txt kvalue.txt

# Rscript:
# (1) Calculate Hypergeometric Test p-value and adjust p-values for multiple testing correction
# (2) Plot barchart and indicate significant samples based on hypergeometric test
# (3) Overwrite .hypTest.txt file - additional columns added (pval, adj.pval, sig)

resultsDirList="*.hypTest.txt"
script="$REPOS/motif_analysis/PROJECT_PAIRED_MOTIFS/exp_enrichment/computeHypTest.R"

for hypTestFile in $resultsDirList; do
	outFile="${hypTestFile/.txt}.pdf"	
	Rscript --quiet $script "inputFile=\"$hypTestFile\"" "outputFileName=\"$outFile\"" "pval=$pval" "numTestFiles=\"$numTestFiles\""
done