#!/usr/bin/perl
#===============================================================================
#
#  FILE:         expEnrichment.pl
#
#  USAGE:        ./expEnrichment.pl [path]
#
#  DESCRIPTION:  description
#
#  OPTIONS:      [path] path to bed files with merged coordinates of motif pairs
# 
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      26-Mar-2013 13:32:47 GMT
#===============================================================================

use strict;
use warnings;

my $dir = $ARGV[0];
opendir DIR1, $dir or die "Couldn't open '$dir'";
my @motifDirList = readdir(DIR1);
closedir DIR1;

my $peakDir = "$ENV{'WORKDIR'}/ANALYSIS_2/1_motif_pair/1_repeat_masker/repeat_removed/";
opendir DIR2, $peakDir or die "Couldn't open '$peakDir'";
my @peakDirList = readdir(DIR2);
closedir DIR2;

my $allPeaksBed = "$ENV{'RESOURCES'}/compendium/mm10/samples_289/repeat_removed_100bp/all_peaks_merged.bed"; # preferably repeat regions removed
my $allPeaksTmp = `wc -l $allPeaksBed`;
my ($allPeaksLen, $pathtmp) = split( '\s', $allPeaksTmp);

my $pval = 0.0001;
my $numTestFiles = 0;

# Prepare log file
my $logFile = "LOG.txt";
open( LOG, ">$logFile" ) or die "Can't open log file !!";

# Run analysis on all significant motif pairs
foreach my $motifFile (@motifDirList) {
	
	# Ignore files beginning with a period
	next if ($motifFile =~ m/^\./);
	# Ignore non motif coordinate bed files
	next if ($motifFile !~ m/.bed$/);

	# Prepare output file
	my $motifid = $motifFile;
	$motifid =~ s/.bed//;
	my $hypFile = $motifid . ".hypTest.txt";
	open( OUT, ">$hypFile" ) or die "Can't open log file !!";
	print OUT "experiment\tq\tm\tn\tk\n";
	print LOG "Testing:", $motifid, "\n";

	# k = number of balls drawn from the urn
	my $k = `intersectBed -a $motifFile -b $allPeaksBed -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq | wc -l`;
		
	# Find enrichment in all experiments in the compendium
	foreach my $peakFile (@peakDirList) {
	
		# Ignore files beginning with a period
		next if ($peakFile =~ m/^\./);
		# Ignore non motif coordinate bed files
		next if ($peakFile !~ m/.bed$/);
		
		my $peakid = $peakFile;
		$peakid =~ s/.bed//;
		print OUT $peakid, "\t";

		# m = number of white balls in the urn
		my $cmd = `intersectBed -a $peakDir$peakFile -b $allPeaksBed -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq > tmp.txt`;
		system($cmd);
		my $tmp1 = `wc -l tmp.txt`;
		my ($m, $path1) = split ( '\s', $tmp1 );
		
		# q = number of white balls drawn without replacement
		my $q = `intersectBed -a $motifFile -b tmp.txt -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq | wc -l`;
		
		# n = number of black balls in the urn
		my $n = $allPeaksLen - $m;
		
		chomp($m);
		chomp($q);
		chomp($n);
		chomp($k);
		
		print OUT $q, "\t", $m, "\t", $n, "\t", $k, "\n";
		$numTestFiles++;
	}

}

opendir DIR1, $dir or die "Couldn't open '$dir'";
my @resultsDirList = readdir(DIR1);
closedir DIR1;

# Rscript:
# (1) Calculate Hypergeometric Test p-value and adjust p-values for multiple testing correction
# (2) Plot barchart and indicate significant samples based on hypergeometric test
# (3) Overwrite .hypTest.txt file - additional columns added (pval, adj.pval, sig)
foreach my $hypTestFile (@resultsDirList) {

	# Ignore files beginning with a period
	next if ($hypTestFile =~ m/^\./);
	# Ignore non motif coordinate bed files
	next if ($hypTestFile !~ m/.hypTest.txt$/);

	my $id = $hypTestFile;
	$id =~ s/.txt//;
	my $outFile = $id . ".pdf";
	my $script = "/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_PAIRED_MOTIFS/exp_enrichment/computeHypTest.R";
	my $cmd = "Rscript --quiet $script 'inputFile=\"$hypTestFile\"' 'outputFileName=\"$outFile\"' 'pval=$pval' 'numTestFiles=$numTestFiles'";
	system($cmd);
	
}

# Remove temporary file
unlink("tmp.txt") or warn "Could not unlink: tmp.txt\n";

exit;