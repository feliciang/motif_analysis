#!/bin/bash
#===============================================================================
#
#  FILE:        getQ.sh
#
#  USAGE:       ./getQ.sh
#
#  DESCRIPTION: Script to get motif-pair coordinates that overlap an experiment.
#				Resulting coordinates from this script is >= q value in *.hypTes
#				t.txt file. This script returns motif-pair coordinates rather
#				than ALL_PEAKS coordinates (as in expEnrichment.pl). 
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      05-Apr-2013
#===============================================================================

peakDir="/home/fn231/pacific/ChIP_Seq_Archive/MOUSE/Compendium/Files/All_current_files/Peak_files/mm10/FINAL_PEAKS/Lifted_from_mm9_FINAL_peaks/"
motifDir="/home/fn231/pacific/ChIP_Seq_Archive/MOUSE/Compendium/Files/All_current_files/working_dir/subset_2/homer_find_subset_merged/barchart/"
bigMatrix="/home/fn231/pacific/Felicia/RESOURCES/compendium/from_david/big_matrix.bed"

experiment=$1
motifPair=$2
outputFile="${motifPair/.bed}_${experiment}.bed"

intersectBed -a "${peakDir}$1.bed" -b $bigMatrix -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq > tmp.txt

# To get motif-pair coordinates:
intersectBed -a "${motifDir}$2.bed" -b tmp.txt -wa -f 1 | sort -n -k 1b,1 -k 2,2 | uniq > $outputFile

# To get ALL_PEAKS coordinate:
#intersectBed -a "${motifDir}$2.bed" -b tmp.txt -wb -f 1 | cut -f 4-6 | sort -n -k 1b,1 -k 2,2 | uniq > $outputFile