#===============================================================================
#
#  FUNCTION:    motifCluster.R
#
#  DESCRIPTION: Hierarchical clustering of distance object obtained from 
#				getSimScoreMat.R
#				
#  ARGUMENTS:
#  d:   
#    dist object
#  output:   
#    output format - 'hclust' or 'newick'
#  cut:
#    logical value indicating if dynamicTreeCut should be applied
#  minCl:
#	 minimum number of clusters for dynamicTreeCut
#
#  RETURN:  	hclust or newick object	
#
#  AUTHOR:      Felicia Ng (fn231)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     6 Aug 2013
#===============================================================================

library(ctc)
library(dynamicTreeCut)
library(moduleColor)

motifCluster <- function(d, hcmethod="average", output="newick", cut=T, cutmethod="hybrid", minCl=5) {

	# Agglomerative hierarchical clustering
	hr <- hclust(d, method = hcmethod, members=NULL)
	
	# Convert dendrogram to newick format (for evolgenius)
	if (output=="newick") {
		nw <- hc2Newick(hr)
		write.table(nw, file="motifCluster.newick.txt", sep="\t", row.names=F, col.names=F, quote=F)
	}
	save(hr, file="motifCluster_hr.RData")
	
	# Find optimal clustering (dynamicTreeCut)
	if (cut==T) {
		
		# Hybrid method
		ds <- 1
		mycl <- cutreeDynamic(hr, minClusterSize=minCl, method=cutmethod, deepSplit=ds, 
								pamStage=T, distM=as.matrix(d), maxPamDist=0, verbose=0)				
		n <- length(unique(mycl))
		
		# Assign colors to clusters
		mycol <- substr(rainbow(n), 1, 7)
		clcol <- labels2colors(mycl, colorSeq=mycol)
		clustcol <- data.frame(id=labels(d), col=clcol)
		
		# Write results to file
		clusttbl <- data.frame(id=labels(d), cluster=mycl, col=clcol)
		write.table(clusttbl, file="cluster_tbl.txt", sep="\t", row.names=F, quote=F)
		
		# Generate annotation file for evolgenius
		tmp <- unique(clusttbl[,2:3])
		s <- sort(as.numeric(tmp[,1]), decreasing=F, index.return=T)
		grpcol <- tmp[s$ix,]
		
		annotfname <- "evolgenius_annot_clust.txt"
		sink(annotfname)
		cat("# color strips\n")
		cat("!groups\t", paste(grpcol[,1], collapse=","), "\n")
		cat("!colors\t", paste(grpcol[,2], collapse=","), "\n")
		cat("!type	strip\n")
		cat("!ShowLegends	1\n")
		cat("!PlotWidth	20\n\n")
		cat("# data\n")
		sink()
		write.table(clustcol, file=annotfname, sep="\t", row.names=F, col.names=F, quote=F, append=T)
	
		# Organize logos into cluster folders
		logoDir <- paste(Sys.getenv("RESOURCES"), "/db/Jaspar_wo_uniprobe/trim_IC0.5_4bp/logos", sep="")
		currDir <- getwd()
		
		for (i in 0:(length(unique(mycl))-1)) {
			
			idx <- which(mycl==i)
			
			if (!is.null(idx)) {
				subDir <- paste("cluster", i, sep="")
				if (file.exists(subDir)){
					destDir <- file.path(currDir, subDir)
				} else {
					dir.create(file.path(currDir, subDir))
					destDir <- file.path(currDir, subDir)
				}
				
				fileids <- labels(d)[idx]
				filenames <- paste(fileids, ".eps", sep="")
				for (j in 1:length(filenames)) {
					file.copy(file.path(logoDir, filenames[j]), destDir)	
				}	
			}
			
		}

	}
	
	# Generate log file with parameters used
	st <- Sys.time()
	st <- gsub(" ", "_", as.character(st))
	st <- gsub(":", "", as.character(st))
	logfilename <- paste("motifCluster_", substr(st, 1, nchar(as.character(st))-2), ".param", sep="")
	sink(logfilename)
	cat("SOURCE DATA\n")
	cat("===========\n")
	cat("Distance object: ", deparse(substitute(d)), "\n")
	cat("# of motifs: ", length(labels(d)), "\n\n")
	cat("HIERARCHICAL CLUSTERING\n")
	cat("=======================\n")
	cat("Method: ", hcmethod, "\n")
	cat("Output: ", output, "\n")
	cat("Cut: ", cut, "\n\n")
	if (cut==T) {
		cat("CUT TREE\n")
		cat("========\n")
		cat("Cut method: ", cutmethod, "\n")
		cat("Min # of clusters: ", minCl, "\n")
		cat("Deep split: ", ds, "\n")
	}	
	sink()
	
}