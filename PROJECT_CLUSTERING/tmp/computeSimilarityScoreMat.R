#===============================================================================
#
#  FUNCTION:    computeSimilarityScoreMat
#
#  DESCRIPTION: Computes all pairwise similarity scores between any 2 pfm.
#				
#  ARGUMENTS:
#  method:   
#	 Metric to be used in similarity measure. Must be one of the following:
#    'pearson' - pearson correlation coefficient
#	 'kullback' - kullback-leibler divergence
#	 'euclidean' - euclidean distance
#
#  RETURN:  	matrix of similarity score
#
#  AUTHOR:      Felicia Ng (fn231)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     3 Jul 2013
#===============================================================================

#library(parallel)

computeSimilarityScoreMat <- function(method, minmatch=2, thr=0.05) {
	
	pfmfilenames <- list.files(pattern=".pfm")
	len <- length(pfmfilenames)
	
	source("/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/pfm2pwm.R")
	
	simmat <- matrix(0, nrow=len, ncol=len)
	
	# Loop through all files
	for (i in 2:len) {
		# Read in frequency matrix of query motif and convert to weight matrix
		querypfm <- read.table(pfmfilenames[i])
		query <- pfm2pwm(querypfm)
		
		# Generate indexes of 2nd motif for pairwise comparison
		idx <- 1:(i-1)
		simvec <- rep(0, length(idx))
		
		# Loop through all files except query and run pairwise comparison 
		# Evaluates all offset values and reverse complement
		for (j in 1:length(idx)) {
			
			# Read in frequency matrix of target motif and convert to weight matrix 
			targetpfm <- read.table(pfmfilenames[i])
			target <- pfm2pwm(targetpfm)
			
			# Generate all possible offset values taking into account 'minmatch'
			negoffset <- -(ncol(pwm2)-minoverlap):-1
			posoffset <- 1:(ncol(pwm1)-minoverlap)
			offsetvec <- c(negoffset, 0, posoffset)
			
			# Results vector: one score for each offset value
			offsetscores <- rep(0, length(offset))
			ct <- 1
			
			# Calculate similarity score for all offset values
			for (offset in offsetvec) {
				
				pwm <- getOverlapPWM(offset, query, target)
				pwmmatch <- apply(pwm, 2, function(x) x[1:4]*x[5:8])  
				matchidx <- which(pwmmatch > thr)
				
				# Calculate similarity score if both matrices have at least 2 matches
				if (matchidx>=minmatch) {
					s <- computeSimScore(pwm)
					offsetscores[ct] <- s
				}
				ct <- ct + 1
			}
			
			# Summarize similarity score - max across all of?
			simscore <- 
			simvec[j] <- simscore
		}
		
		simmat[idx,i] <- simvec
	}
	
}

# Generate a pwm where query and target overlaps based on a given offset
getOverlapPWM <- function (v, q, t) {
	
	if (v==0) {
		end <- min(ncol(q), ncol(t))
		pwm <- rbind(q[ , 1:end], t[ , 1:end])
	}else if (v > 0) {
		start1 <- v + 1
		end <- ncol(q) - v
		end2 <- min(end, ncol(t))
		pwm <- rbind(q[ , start1:ncol(q)], t[ , 1:end2])
	}else if (v < 0 ) {
		end <- ncol(t) - v
		end1 <- min(end, ncol(q))
		start2 <- v + 1
		pwm <- rbind(q[ , 1:ncol(q)], t[ , start2:ncol(t)])
	}
	
	return(pwm)
	
}

computeSimScore <- function(mat, method) {

	if (method=="euclidean") {
		
	}else if (method=="kullback") {
	
	}else if (method=="pearson") {
		s <- apply(mat, 2, function(x) cor(x[1:4], x[5:8])) # correlation for each aligned bp
	}
	
	s.norm <- sum(s)/ncol(mat)
	return(s)
	
}