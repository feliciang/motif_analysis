#===============================================================================
#
#  FUNCTION:    filterpwm
#
#  DESCRIPTION: For Tulsi's project. Script to filter reliable PWMS using the
#				criteria icthr and length of PWM.
#				
#  ARGUMENTS:
#  icthr:   
#    information content threshold
#  len:   
#    minimum length of 'core' motif
#  format:
#    pfm or pwm
#  outdir:
#    path to where results will be saved
#
#  RETURN:  	One .pwm file for each input file
#
#  AUTHOR:      Felicia Ng (fn231), Tulsi Patel (tp390)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     2 June 2014
#===============================================================================

filterpwm <- function(icthr=1, len=3, format="pwm", outdir=Sys.getenv("PWD")) {
	
	bg=c(0.25, 0.25, 0.25, 0.25)
	
	if (format=="pfm") {
		inputfilenames <- list.files(pattern=".pfm")
		source(paste(Sys.getenv("PACIFIC"), "/Felicia/repos/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/pfm2pwm.R"), sep="")	
	}else if (format=="pwm") {
		inputfilenames <- list.files(pattern=".pwm")
	}else {
		warning("Format not recognized!")
	}
	
	for (i in 1:length(inputfilenames)) {
		
		if (format=="pfm") {
			pfm <- read.table(inputfilenames[i])
			pwm <- pfm2pwm(pfm)	
		}else if (format=="pwm") {
			pwm <- read.table(inputfilenames[i])
		}
		
		# Calculate information content for each position
		logIC <- apply(log2(pwm/bg)*pwm, 2, function(x) sum(na.omit(x)))
		idxhi <- which(logIC>=icthr)
		
		if (length(idxhi)>0) {
			
			diff <- rle(diff(idxhi))
			runlen <- max(diff$lengths[which(diff$values==1)])
			
			if (runlen>=len) {
				# Add here
			}
		}
		
	}
	
}