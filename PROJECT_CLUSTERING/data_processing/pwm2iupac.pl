#!/usr/bin/perl
#===============================================================================
#
#  FILE:         pwm2iupac.pl
#
#  USAGE:        ./pwm2iupac.pl [MEME/PWM file] [format of input file]
#
#  DESCRIPTION:  Reads in Jaspar motifs in MEME/PWM format and converts each 
#				 matrix to IUPAC codes. According to the Bioperl manual, 
#				 probabilities less than 0.05 are ignored.
#
#  OPTIONS:      
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      12-Jul-2013 14:53:08 GMT
#===============================================================================

use strict;
use warnings;
use Bio::Matrix::PSM::SiteMatrix;

#my $file = "/home/fn231/pacific/Felicia/RESOURCES/db/Jaspar/trim_IC0.5_4bp/meme_format/Jaspar_wo_uniprobe_MEME_trim.txt";
my $file = $ARGV[0];
my $format = $ARGV[1];
my @data = get_file_data($file);
my $ct = 0;
my $width = 0;
my @a = ();
my @c = ();
my @g = ();
my @t = ();

if ($format =~ /meme/i) {
	
	foreach my $line (@data) {

		my $motifid;
	
		if ($line =~ m/^MOTIF\s+([A-Za-z0-9._-]+)\s+[A-Za-z0-9._-]+/) {
			$motifid = $1;
			print $motifid, "\t";
		}elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= (\d+)/) {
			$width = int($1);
		}elsif ($line =~ m/^\s{2}(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/) {
			push(@a, $1);
			push(@c, $2);
			push(@g, $3);
			push(@t, $4);
			$ct++;
			
			if ($width==$ct) {
				my $iupac = getIUPAC(\@a, \@c, \@g, \@t);
				print $iupac, "\n";
				$ct = 0;
				@a = ();
				@c = ();
				@g = ();
				@t = ();
			}
		}else {
			next;
		}

	}
		
}else if ($format =~ /pwm/i) {
	
	foreach my $line (@data) {

		my $motifid;

		if ($line =~ m/^>\s*([A-Za-z0-9._-]+)/) {
			$motifid = $1;
			print $motifid, "\t";
		}elsif ($line =~ m/^(\d.\d+)\s+/) {
			if ($ct==0) {
				@a = split("\t", $line);
			}else if ($ct==1) {
				@c = split("\t", $line);
			}else if ($ct==2) {
				@g = split("\t", $line);
			}else if ($ct==3) {
				@t = split("\t", $line);
			}
			$ct++;
			my $iupac = getIUPAC(\@a, \@c, \@g, \@t);
			print $iupac, "\n";
			$ct = 0;
			@a = ();
			@c = ();
			@g = ();
			@t = ();
		}else {
			next;
		}

	}
		
}

sub getIUPAC {
	
	my ($a, $c, $g, $t, $mid) = @_;
	
	my %param=(-pA=>$a,-pC=>$c,-pG=>$g,-pT=>$t,-id=>$mid);
	my $site=Bio::Matrix::PSM::SiteMatrix->new(%param);
	
	my $iupac=$site->IUPAC;
	return $iupac;
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;

}

exit;