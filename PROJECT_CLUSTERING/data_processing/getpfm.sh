#!/bin/bash

idlist=${1:-/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/6_process_distance/uniq_motif_id.txt}
pfmdir="/home/fn231/pacific/Felicia/RESOURCES/db/Jaspar/FlatFileDir"

while read LINE
do
	cp "$pfmdir/$LINE.pfm" ./
done < $idlist