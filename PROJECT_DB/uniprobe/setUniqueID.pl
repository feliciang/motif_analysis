#!/usr/bin/perl
#
# Change MOTIF line and set unique ID
#
# Felicia Ng
# 20 Nov 2012

use strict;
use warnings;
use myutils;

my $dir = $ARGV[0];
opendir(DIR, $dir) or die $!;
my $dataset = "SCI09";
my $i = 1;

while (my $file = readdir(DIR)) {

	# Ignore non XML files
	next if ($file !~ m/meme.txt$/);
		
	my @data = get_file_data($file);
	
	# Remove file extension
	my @fname = split ( /\./, $file );
	my $fileext = "." . pop(@fname);
	$file =~ s/$fileext//g;
	
	my $outfile = $file. "_mod.txt";
	open(OUT, ">$outfile");
	
	foreach my $line (@data) {
		if ($line =~ m/^MOTIF/) {
			my @elements = split ( /\s+/, $line );
			my $id = "UP." . $dataset . "." . $i;
			my $name = $elements[4];
			print(OUT "MOTIF $id $name\n");
		}else {
			print(OUT $line);
		}
	}
	
	close(OUT);
	$i++;
	
}