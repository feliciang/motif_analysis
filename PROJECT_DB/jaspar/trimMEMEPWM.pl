#!/usr/bin/perl
#===============================================================================
#
#  FILE:        trimPWM.pl
#
#  USAGE:       ./trimPWM.pl [pwm_file]
#
#  DESCRIPTION: This program takes in Jaspar motifs in MEME format (1 file) and
#				trims flanking regions based on a given Information Content
#				threshold. Trimmed PWMs must have at least a user defined length.
#
# OPTIONS:      [pwm_file] Jaspar motifs in MEME format
#				[out_file] Name of output file (MEME format)
#				[ic_thr]   Information content threshold (default=0.5)
#				[len]	   Minimum length of 'core' motif (default=4)
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      19-Mar-2013 10:52:13 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;

my $jFile = $ARGV[0];
my @jData = get_file_data($jFile);

my $outFile = $ARGV[1];
open( OUT, ">$outFile" ) or die "Can't open output file - $outFile !";
printHeader();

my $ICthr = $ARGV[2] || 0.5;
my $len = $ARGV[3] || 4;

my $script = "/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_DB/jaspar/trimPWM.R";
my $idx = 0;
my $print = 0;
my $jasparid;
my $jasparname;
my $nsites;
my $cmd;

foreach my $line (@jData) {
	
	if ($line =~ m/^MOTIF\s+([A-Za-z0-9._-]+)\s+([A-Za-z0-9._-]+)/){
		$jasparid = $1;
		$jasparname = $2;
	}elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= \d+ nsites= (\d+) E= \d+/) {
		$nsites = $1;
	}elsif ($line =~ m/^\s{2}(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/ && $idx eq 0) {
		$idx = 1;
		$print = 1;
		my $tmpFile = "tmp.txt";
		open( TMP, ">$tmpFile" ) or die "Can't open tmp file !";
		print TMP $1 , "\t", $2, "\t", $3, "\t", $4, "\n";
	}elsif ($line =~ m/^\s{2}(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/ && $idx eq 1) {
		print TMP $1 , "\t", $2, "\t", $3, "\t", $4, "\n";
	}elsif ($line =~ /^\s*$/ && $idx eq 1) {
		$idx = 0;
		$cmd = "Rscript --quiet $script 'icthr=$ICthr' 'jasparid=\"$jasparid\"' 'jasparname=\"$jasparname\"' 'nsites=$nsites' 'len=$len'";
		system($cmd);
		if (-e "newPWM.txt") {
			my @pwmData = get_file_data("newPWM.txt");
			foreach my $line2 (@pwmData) {
				if ($line2 =~ /^MOTIF/) {
					print OUT $line2, "\n";
				}elsif ($line2 =~ /^letter/) {
					print OUT $line2;	
				}elsif ($line2 =~ m/^(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/) {
					printf OUT "  %.6f\t  %.6f\t  %.6f\t  %.6f\n", $1, $2, $3, $4;
				}
			}	
			print OUT "\n";	
			unlink "newPWM.txt";
		}
		$print = 0;
	}
	
}

if ($print eq 1) {
	$cmd = "Rscript --quiet $script 'icthr=$ICthr' 'jasparid=\"$jasparid\"' 'jasparname=\"$jasparname\"' 'nsites=$nsites' 'len=$len'";
	system($cmd);
	if (-e "newPWM.txt") {
		my @pwmData = get_file_data("newPWM.txt");
		foreach my $line2 (@pwmData) {
			if ($line2 =~ /^MOTIF/) {
				print OUT $line2, "\n";
			}elsif ($line2 =~ /^letter/) {
				print OUT $line2;	
			}elsif ($line2 =~ m/^(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/) {
				printf OUT "  %.6f\t  %.6f\t  %.6f\t  %.6f\n", $1, $2, $3, $4;
			}
		}	
		print OUT "\n";
		unlink "newPWM.txt";	
	}	
}

close(OUT);

# Clear temporary files
my $clr1 = "rm tmp.txt";
system($clr1);

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

sub printHeader {
	
	# Print header for meme output file
	print OUT "MEME version 4.4\n\n";
	print OUT "ALPHABET= ACGT\n\n";
	print OUT "strands: + -\n";
	print OUT "Background letter frequencies (from uniform background):\n";
	print OUT "A 0.25000 C 0.25000 G 0.25000 T 0.25000\n\n";

}

exit;