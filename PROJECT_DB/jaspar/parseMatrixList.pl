#!/usr/bin/perl
# Parse Jaspar file
#
# Felicia Ng
# 13 July 2012

use strict;
use warnings;
use myutils;
use Switch;

# Jaspar matrix list file
my $matrixfile = "/home/fn231/pacific/Felicia/RESOURCES/db/JASPAR/FlatFileDir/list/matrix_list.txt";
my @matrixdata = get_file_data($matrixfile);

my $familyfile = "/home/fn231/pacific/Felicia/RESOURCES/db/JASPAR/FlatFileDir/list/family_short_form.txt";
my @familydata = get_file_data($familyfile);
my %familyhash = ();
foreach my $line (@familydata) {
		my @element = split ( '\t', $line );
		my $name = $element[0];
		my $sform = $element[1];
		$familyhash{$name} = $sform;
}

print "id\tname\tclass\tcollection\tfamily\tshort_form\tconsensus\tmedline\tspecies\n";

foreach my $line (@matrixdata) {
	
	my $id = "NA";
	my $name = "NA";
	my $class = "NA";
	my $collection = "NA";
	my $family = "NA";
	my $familyshort = "NA";
	my $consensus = "NA";
	my $medline = "NA";
	my $species = "NA";

	my @element = split ( '\t', $line );
	$id = $element[0];
	$name = $element[2];
	$class = $element[3];

	my @element2 = split ( ';', $element[4]);
	
	foreach (@element2) {
		$_ =~ s/"//g;
 		switch (trim($_)) {
 			case /collection/		{ $_=~ s/collection//; $collection = trim($_) ; }
 			case /family/			{ $_=~ s/family//; $family = trim($_) ; }
 			case /consensus/		{ $_=~ s/consensus//; $consensus = trim($_) ; }
 			case /medline/			{ $_=~ s/medline//; $medline = trim($_) ; }
 			case /species/			{ $_=~ s/species//; $species = trim($_) ; }
 		}
	}
	
	if ( exists $familyhash{$family} ) {
		$familyshort = trim($familyhash{$family});
	}

	print $id, "\t", $name, "\t", $class, "\t", $collection, "\t", $family, "\t", $familyshort, "\t", $consensus,  "\t", $medline, "\t", $species, "\n";
}

exit;