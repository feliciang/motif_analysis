#!/usr/bin/perl

use strict;
use warnings;
use myutils;

my $file = $ARGV[0];
my @data = get_file_data($file);
	
# Remove file extension
my @fname = split ( /\./, $file );
my $fileext = "." . pop(@fname);
$file =~ s/$fileext//g;

my $outfile = $file. "_renamed.txt";
open(OUT, ">$outfile");

my $familyhashref = loadJasparInfo();
my %familyhash = %{$familyhashref};

foreach my $line (@data) {
	if ($line =~ m/^MOTIF/) {
		my @elements = split ( /\s+/, $line );
		my $id = $elements[1];
		my $name = $familyhash{$id};
		print(OUT "MOTIF $id $name \n");
	}else {
		print(OUT $line);
	}
}

close(OUT);

sub loadJasparInfo {
	# Store jaspar motif information in a hash
	my %familyhash = ();
	
	my $jasparfile = $ENV{'HOME'} . "/pacific/Felicia/RESOURCES/db/JASPAR/FlatFileDir/list/parsed_matrix_list.txt";
	unless( open(FILE, $jasparfile) ) {
       	print STDERR "Cannot open file \"$jasparfile\"\n\n";
       	exit;
    }
    my @jaspardata = <FILE>;
	close FILE;
	    
	foreach my $line (@jaspardata) {
		my @element = split ( '\t', $line );
		my $id = $element[0];
		my $name = $element[1];
		$familyhash{$id} = $name;
	}
	return \%familyhash;
}