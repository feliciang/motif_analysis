#!/usr/bin/perl

use strict;
use warnings;
use Cwd;

my $dir = $ARGV[0];
opendir(DIR, $dir) or die $!;

my %results = ();

while (my $file = readdir(DIR)) {

	# Ignore files beginning with a period
	next if ($file =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$file");

	# Obtain file data
	my @data = get_file_data($file);
	
	# Remove file extension
	my @fname = split ( /\./, $file );
	my $fileext = "." . pop(@fname);
	$file =~ s/$fileext//g;
	
	foreach my $line (@data) {
		if ($line =~ m/^#/) {
			next;
		}else {
			my @element = split ( '\t', $line );
			my $motif = trim($element[0]);
			my $scoretmp = trim($element[3]);
			my $score;	
			if (abs($scoretmp)==0) {		# Correct scores in centrimo results (some zero values are -0)
				$score = 0;	
			}elsif (abs($scoretmp) > 0) {	# Convert scores to positive scores (-log p-value)
				$score = $scoretmp*-1;
			}else {
				$score = $scoretmp;
			}
			$results{$motif}{$file}= $score;
		}
	}

}

closedir(DIR);

# Print results stored in hash
my @motiftmp = sort keys %results;
my @samplestmp = sort keys %{ $results{$motiftmp[0]} };
print "motif";
foreach my $sample (@samplestmp) {
	print "\t", $sample;
}
print "\n";

foreach my $motif ( sort keys %results ) {
    print "$motif";
    foreach my $sample ( sort keys %{ $results{$motif} } ) {
        	print "\t$results{$motif}{$sample}";
    }
    print "\n";
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

sub trim {

	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	chomp $string;
	return $string;

}

exit;