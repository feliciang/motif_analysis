#!/usr/bin/perl

use strict;
use warnings;
use XML::Simple;

my $dir = $ARGV[0];
opendir(DIR, $dir) or die $!;
my $motiftype= $ARGV[1];
my $header = 0;
my $familyhashref;

my $ethr = 0.01;

# Go through all files in $dir
while (my $file = readdir(DIR)) {

	# Ignore files beginning with a period
	next if ($file =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$file");
	# Ignore non XML files
	next if ($file !~ m/.xml$/);
	
	if ($motiftype eq "nomatch") {
		getOrigFreqMatrix($file, "nomatch", $ethr);	
	}elsif ($motiftype eq "match") {
		getOrigFreqMatrix($file, "match", $ethr);	
	}
	
}

sub printHeader {
	
	# Print header for meme output file
	print "MEME version 4.4\n\n";
	print "ALPHABET= ACGT\n\n";
	print "strands: + -\n";
	print "Background letter frequencies (from uniform background):\n";
	print "A 0.25000 C 0.25000 G 0.25000 T 0.25000\n\n";

}

sub printMotif2StdOut {
	
	my ($name, $href) = @_;

	print "MOTIF ", $name, "\n\n";
	print "letter-probability matrix: alength= 4 w= ", $href->{'motif'}->{'length'}, " nsites= ", $href->{'motif'}->{'nsites'}, " E= 0\n";
		
	foreach my $posref (@{$href->{'motif'}->{'pos'}}) {
		printf "  %.6f\t  %.6f\t  %.6f\t  %.6f\n", $posref->{'A'}, $posref->{'C'}, $posref->{'G'}, $posref->{'T'};
	}
	print "\n";
	
}

sub getMotif {
	
	my ($motifhref, $mode) = @_;
	my $motifname;

	# Get all motif instances where there is NO match in Jaspar
	if ( (!exists $motifhref->{'match'}) && ($mode eq "nomatch") ) {
		$motifname = $motifhref->{'motif'}->{'name'} . "_novel";
		printMotif2StdOut($motifname, $motifhref);			
	# Get all motif instances where there IS match in Jaspar
	}elsif ( (exists $motifhref->{'match'}) && ($mode eq "match") ) {
		$motifname = $motifhref->{'motif'}->{'name'} . "_known";
		printMotif2StdOut($motifname, $motifhref);
	}	
	
}

sub getOrigFreqMatrix {
	
	my ($filename, $mode, $et) = @_;	
	
	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($filename);	
	my %qfile =  %{$doc->{queries}->{query_file}};
	my $q = $qfile{'query'};
			
	if ($header==0) {
		printHeader();
		$header++;	
	}
	
	if (ref($q) eq "HASH" && $q->{'motif'}->{'evalue'}<=$et) {
		my $motifhref = $q;
		getMotif($motifhref, $mode);
	}elsif (ref($q) eq "ARRAY") {
		foreach my $motifhref (@$q) {
			if ($motifhref->{'motif'}->{'evalue'}<=$et) {
				getMotif($motifhref, $mode);	
			}
		}
	}else {
		next;
	}
	
}		

exit;