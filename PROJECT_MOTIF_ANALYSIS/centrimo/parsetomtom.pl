#!/usr/bin/perl

use strict;
use warnings;
use XML::Simple;

my $dir = $ARGV[0];
my $motiftype= $ARGV[1];
my $header = 0;
my $familyhashref;

opendir(DIR, $dir) or die $!;

# Go through all files in $dir
while (my $file = readdir(DIR)) {

	# Ignore files beginning with a period
	next if ($file =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$file");
	# Ignore non XML files
	next if ($file !~ m/.xml$/);
	
	if ($motiftype eq "jaspar") {
		getJaspFreqMatrix($file);
	}elsif ($motiftype eq "nomatch") {
		getOrigFreqMatrix($file, "nomatch");	
	}elsif ($motiftype eq "match") {
		getOrigFreqMatrix($file, "match");	
	}
	
}

sub printHeader {
	
	# Print header for meme output file
	print "MEME version 4.4\n\n";
	print "ALPHABET= ACGT\n\n";
	print "strands: + -\n";
	print "Background letter frequencies (from uniform background):\n";
	print "A 0.25000 C 0.25000 G 0.25000 T 0.25000\n\n";

}

sub loadJasparInfo {
	# Store jaspar motif information in a hash
	my %familyhash = ();
	
	my $jasparfile = $ENV{'HOME'} . "/pacific/Felicia/RESOURCES/db/JASPAR/FlatFileDir/list/parsed_matrix_list.txt";
	unless( open(FILE, $jasparfile) ) {
       	print STDERR "Cannot open file \"$jasparfile\"\n\n";
       	exit;
    }
    my @jaspardata = <FILE>;
	close FILE;
	    
	foreach my $line (@jaspardata) {
		my @element = split ( '\t', $line );
		my $id = $element[0];
		my $sform = $element[5];
		$familyhash{$id} = $sform;
	}
	return \%familyhash;
}

sub getJaspFreqMatrix {
	
	my $filename = $_[0];
	my %motifhash = ();
	my $jasparmatrixfile= $ENV{'HOME'} . "/pacific/Felicia/RESOURCES/db/JASPAR/matrix_only/matrix_only.txt";

	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($filename);
	  
	my %tfile =  %{$doc->{targets}->{target_file}};
	
	if (exists($tfile{'motif'})) {
		
			if ($header==0) {
				printHeader();
				$header++;	
			}
			
			my $t = $tfile{'motif'};
			
			foreach my $jasparid (keys %{$t}) {

			my $line = `grep $jasparid $jasparmatrixfile`;
			my $motifname = ( split ( /\s+/, $line ) )[1];
			
			if (!exists $motifhash{$jasparid}) {
				$motifhash{$jasparid}=1;
	
				# Get all motif instances where there is a match in Jaspar
				print "MOTIF ", $jasparid, "_", $motifname, "\n\n";
				print "letter-probability matrix: alength= 4 w= ", $t->{$jasparid}->{'length'}, " nsites= ", $t->{$jasparid}->{'nsites'}, " E= 0\n";
				
				foreach my $posref (@{$t->{$jasparid}->{'pos'}}) {
					printf "  %.6f\t  %.6f\t  %.6f\t  %.6f\n", $posref->{'A'}, $posref->{'C'}, $posref->{'G'}, $posref->{'T'};
				}
				print "\n";
			}else {
				next;
			}
		}	
	}
		
}

sub printMotif2StdOut {
	
	my ($name, $href) = @_;

	print "MOTIF ", $name, "\n\n";
	print "letter-probability matrix: alength= 4 w= ", $href->{'motif'}->{'length'}, " nsites= ", $href->{'motif'}->{'nsites'}, " E= 0\n";
		
	foreach my $posref (@{$href->{'motif'}->{'pos'}}) {
		printf "  %.6f\t  %.6f\t  %.6f\t  %.6f\n", $posref->{'A'}, $posref->{'C'}, $posref->{'G'}, $posref->{'T'};
	}
	print "\n";
	
}

sub getMotif {
	
	my ($motifhref, $motifid, $mode) = @_;
	
	my %familyhash;
	
	if ($mode eq "match") {
		$familyhashref = loadJasparInfo();
		%familyhash = %{$familyhashref};	
	}	
	
	# Get all motif instances where there is NO match in Jaspar
	if ( (!exists $motifhref->{'match'}) && ($mode eq "nomatch") ) {
		
		my $motifname = $motifid . "_" . $motifhref->{'motif'}->{'id'};
		printMotif2StdOut($motifname, $motifhref);
			
	# Get all motif instances where there IS match in Jaspar
	}elsif ( (exists $motifhref->{'match'}) && ($mode eq "match") ) {
		
		my $jaspshortid;		
		# Concatenate all matching Jaspar IDs and use as motif ID
		if (ref($motifhref->{'match'}) eq "HASH") {
			my $tmpid = $motifhref->{'match'}->{'target'};
			my @elements = split ("_", $tmpid);
			my $jasparid = pop @elements;
			$jaspshortid = $familyhash{$jasparid};
		}elsif (ref($motifhref->{'match'}) eq "ARRAY") {
			my @mergedid;
			foreach my $target ( @{$motifhref->{'match'}} ) {
				my $tmpid = $target->{'target'};
				my @elements = split ("_", $tmpid);
				my $jasparid = pop @elements;
				my $shortname = $familyhash{$jasparid};
				push(@mergedid, $shortname);
			}
			my %seen = ();
			my @uniqname = grep { ! $seen{$_} ++ } @mergedid;
			$jaspshortid = join("_", @uniqname);
		}
	
		my $motifname = $jaspshortid . "_" . $motifid . "_". $motifhref->{'motif'}->{'id'};
		printMotif2StdOut($motifname, $motifhref);
		
	}
}

sub getOrigFreqMatrix {
	
	my ($filename, $mode) = @_;	
	
	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($filename);	
	my %qfile =  %{$doc->{queries}->{query_file}};
	my $q = $qfile{'query'};
	
	# Remove file extension and use file name as motif ID
	my @fname = split ( /\./, $filename );
	my $fileext = "." . pop(@fname);
	(my $motifid = $filename) =~ s/$fileext//g;
			
	if ($header==0) {
		printHeader();
		$header++;	
	}
	
	if (ref($q) eq "HASH") {
		my $motifhref = $q;
		getMotif($motifhref, $motifid, $mode);
	}elsif (ref($q) eq "ARRAY") {
		foreach my $motifhref (@$q) {
			getMotif($motifhref, $motifid, $mode);
		}
	}
	
}		

exit;