#!/bin/bash
#
# Rename motifs in each meme file based on file name and then merge all files - used as input for STAMP 
#
# Felicia Ng 
# 5 Nov 2012

mkdir merged

for i in *.txt
do
	if [ -f $i ] ; then
		meme2meme $i > merged/${i/.txt}.meme
	fi
done

cd merged

for j in *.meme
do
	perl /home/fn231/pacific/Felicia/SCRIPTS/PROJECT_MOTIF_ANALYSIS/meme/changeMotifName.pl $j
done

meme2meme *_renamed.txt > merged.meme.txt

rm *.meme *_renamed.txt