#!/bin/bash
#===============================================================================
#
#  FILE:        checkPeakBed.sh
#
#  USAGE:       ./checkPeakBed.sh [path_to_bed_files]
#
#  DESCRIPTION: Sanity check for peak files (.bed).  Run before repeat masking.
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      20-Nov-2013
#===============================================================================

# Get all bed file names
ls *.bed > bedfilenames.txt 
outFile="checkPeakBed.out"

# Check if peak files have been analyzed
grep -f bedfilenames.txt /home/fn231/results/gottgens/felicia/COMPENDIUM/analyzed_peak_files.txt > motifanalysisdone.txt
ndone=`wc motifanalysisdone.txt | awk 'BEGIN{FS=" "} {print $1}'`
if [ ${ndone} -gt 0 ]; then	
	printf "Motif analysis have been performed for these samples:\n" >> $outFile
	cat motifanalysisdone.txt >> $outFile
	printf "\n\n" >> $outFile
fi

# Check properties of each bed file
while read bedFile;
do
	echo "Checking file: $bedFile"
	printf "%s\t" $bedFile >> $outFile
	
	numpeaks=$(wc $bedFile | awk '{print $1}')
	len=$(grep -v "track" $bedFile | awk '{print $3-$2}' | sort| uniq )
	numlen=$(grep -v "track" $bedFile | awk '{print $3-$2}' | sort | uniq | wc | awk '{print $1}')
	pattern=" "
	numdup=$(grep "{$bedFile/.bed}" bedfilenames.txt | wc | awk '{print $1}')
	sample="${bedFile/.bed}"
	sqlstatement="\"select status from codex where filename='$sample';\""
	status=$(ssh -n fn231@tobias.cscr.cam.ac.uk "mysql -B -Dbioinformatics -u fn231 -pG0tt3ns -e $sqlstatement" | grep -v "status")
	nzero=$(awk '$2<1 {print $0}' $bedFile | wc | awk '{print $1}')
	
	# Check that there are at least 50 peaks
	if [[ $numpeaks -lt 50 ]]; then
		
		printf "too few peaks; " >> $outFile
	
	# Check that all regions are 400bp	
	elif [[ "$numlen" -eq 1 && "$len" -ne 400 ]]; then

		printf "not 400bp; " >> $outFile
	
	elif [ "$numlen" -gt 1 ]; then
		
		printf "non-unique length; " >> $outFile
	
	# Check filename: spaces, duplicate of another file
	elif [[ "$bedFile" =~ "$pattern" ]]; then
		
		printf "file name has spaces; "  >> $outFile
		
	elif [ "$numdup" -gt 1 ]; then
		
		printf "duplicated; " >> $outFile
	
	# Check database record: new_staging_data??
	elif [ -z "$status" ]; then
		
		printf "not in DB; " >> $outFile
		
	elif [[ "$status" && "$status" != "done" ]]; then
		
		printf "not done; " >> $outFile
	
	# Check that all coordinates start with 1
	elif [[ $nzero -gt 0 ]]; then
			
		printf "zero start; " >> $outFile 	
	
	else
		
		printf "OK" >> $outFile	

	fi
	
	printf "\n" >> $outFile
	
done <bedfilenames.txt

rm bedfilenames.txt motifanalysisdone.txt