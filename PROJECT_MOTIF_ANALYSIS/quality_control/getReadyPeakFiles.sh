#!/bin/bash

destDir="/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10_reprocessed/peak_bed/"
samples="/home/fn231/pacific/Felicia/RESOURCES/compendium/staging/131106/ready_131106.txt"

while read LINE
do
	mv "${LINE}.bed" $destDir
done <$samples