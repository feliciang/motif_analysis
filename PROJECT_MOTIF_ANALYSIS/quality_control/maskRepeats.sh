#!/bin/bash
#===============================================================================
#
#  FILE:        maskRepeats.sh
#
#  USAGE:       ./maskRepeats.sh [path_to_bed_files] [mask_threshold]
#
#  DESCRIPTION: Runs RepeatMasker on ChIP-seq peak regions and removes sequences
#				with masked regions above a user given threshold. This program
#				takes in .bed files, converts regions to 100bp size and homer
#				format and extract fasta sequences.  The fasta file is used as
#				input to the RepeatMasker program.
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      02-Sep-2013
#===============================================================================

# User input variable
maskthr=${1:-40}
genome=$2

# Create results directory
resultdir=${3:-"$WORKDIR/COMPENDIUM/pipeline_mm10_reprocessed/1_repeat_masker"}
bed100dir="$resultdir/bed100/"
fastadir="$resultdir/fasta/"
repmaskdir="$resultdir/repeat_masker/"
removeddir="$resultdir/repeat_removed/"
homerdir="$resultdir/homer100_workdir/"
if [ ! -d "$bed100dir" ] ; then
	mkdir $bed100dir
fi
if [ ! -d "$fastadir" ] ; then
	mkdir $fastadir
fi
if [ ! -d "$repmaskdir" ] ; then
	mkdir $repmaskdir
fi
if [ ! -d "$removeddir" ] ; then
	mkdir $removeddir
fi
if [ ! -d "$homerdir" ] ; then
	mkdir $homerdir
fi

# Process all *.bed files
for file in *.bed;
do
	echo "Masking file:  $file"
	
	faFile="${file/.bed}.fasta"
	homerFile="${file/.bed}.homer.txt"
	
	# Process bed files: Resize to 100bp, extract fasta sequence
	grep -v "track name" $file > input.bed
	perl "$REPOS/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/make_bed.pl" input.bed tmp.bed 100
	sort -n -k 1b,1 -k 2,2 tmp.bed | uniq > "$bed100dir$file"
	
	if [ "$genome" == "mm10" ]; then
		genomefasta="$RESOURCES/reference_genomes/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa"
	elif [ "$genome" == "hg19" ]; then
		genomefasta="$RESOURCES/reference_genomes/Homo_sapiens/UCSC/hg19/Sequence/WholeGenomeFasta/genome.fa"
	fi	
	bedtools getfasta -fi $genomefasta -bed "$bed100dir$file" -fo "$fastadir$faFile"
	
	# Run RepeatMasker
	if [ "$genome" == "mm10" ]; then
		RepeatMasker -engine crossmatch -species mouse -dir . "$fastadir$faFile" > "$file.repeatmasker.out"
	elif [ "$genome" == "hg19" ]; then
		RepeatMasker -engine crossmatch -species human -dir . "$fastadir$faFile" > "$file.repeatmasker.out"
	fi
	
	# Delete unneccesary files
	rm "$faFile.log" "$faFile.masked"		
	if [ -f "$faFile.cat" ]; then
		rm "$faFile.cat"
	elif [ -f "$faFile.cat.gz" ]; then
		rm "$faFile.cat.gz"
	fi

	if grep -q "There were no repetitive sequences detected" $file; then
		rm "$file.repeatmasker.out" "$faFile.out"
		cp $file $removeddir		
	else
		# Organize RepeatMasker output files
		mv "$faFile.tbl" "$faFile.out" "$file.repeatmasker.out" $repmaskdir

		# Create .bed file of filtered ID sequences (masked sequences above threshold are removed)
		script="$REPOS/motif_analysis/PROJECT_MOTIF_ANALYSIS/quality_control/filterMaskedSeq.R"
		Rscript --quiet $script "$repmaskdir$faFile.out" $bed100dir$file $removeddir $maskthr
		
		rm tmp.bed input.bed
	fi
	
	# Convert bed file to homer format
	perl "$REPOS/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/bed2homer.pl" $removeddir$file $homerdir$homerFile
	
done