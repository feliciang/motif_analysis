#!/bin/bash
#
# Draw motif logo using MEME's ceqlogo function 
#
# Felicia Ng 
# 3 July 2012

grep "MOTIF" $1 > motifname.txt
awk 'BEGIN{FS="_| ";} {print $2}' motifname.txt > motifid.txt

i=1
while read LINE
do  
	logofname=$LINE".eps"
	ceqlogo -i$((i++)) $1 -o $logofname
	echo $i
done <motifid.txt

rm motifname.txt motifid.txt