#!/bin/bash
#
# Given a list of Jaspar motif IDs as input, this script draws motif logos using the ceqlogo function (MEME suite) 
#
# Felicia Ng 
# 21 Nov 2012

if [ $# -ne 2 ]
then
	echo "error: provide (1) file with motif names, and (2) meme file"
	exit 1
else
	motifnames=$1
	memefile=$2
fi

echo "Using motif names from: "$motifnames"\n"
echo "Using meme file: "$memefile"\n"

while read LINE
do  
	n=`(grep -w -n $LINE $motifnames | awk -F ":" '{print $1}')`
	logofname=$LINE".eps"
	ceqlogo -i$n $memefile -o $logofname
	echo "Drawing motif #"$n" $LINE"
done <$motifnames

# All Jaspar motifs:
#/home/fn231/pacific/Felicia/RESOURCES/db/UniProbe/Uniprobe_ALL_MEME_names.txt
#/home/fn231/pacific/Felicia/RESOURCES/db/JASPAR/meme_format/Jaspar_ALL_motif_names.txt
# All Uniprobe motifs:
#/home/fn231/pacific/Felicia/RESOURCES/db/UniProbe/Uniprobe_ALL_MEME.txt
#/home/fn231/pacific/Felicia/RESOURCES/db/JASPAR/meme_format/Jaspar_ALL_MEME.txt