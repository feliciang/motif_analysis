#!/bin/bash

for bedFile in *.bed; do
	
	pngFile="${bedFile/.bed}.png"
	
	# convert bed to homer format
	/home/fn231/pacific/Felicia/repos/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/bed2homer.pl $bedFile tmp.homer.txt ignorestrand

	# extract fasta sequence
	homerTools extract tmp.homer.txt /home/Programs/HOMER/data/genomes/mm10/ -fa > seq.fasta
	
	# draw logo
	/home/Programs/weblogo-3.3/weblogo --format png -n 100 < seq.fasta > $pngFile
	
done

# remove temp files
rm tmp.homer.txt seq.fasta