#!/usr/bin/perl
#===============================================================================
#
#  FILE:        batchhomerfind.pl
#
#  USAGE:       ./batchhomerfind.pl [path]
#
#  DESCRIPTION: Script to run HOMER -find analysis on all peak files in the
#				compendium against matches to known motifs
#
# OPTIONS:      [path] path to TOMTOM xml files
#				[motifstotest] file listing motifs to test
#				[findsize] size of region to be used in findMotifsGenome.pl
#							(default: 150)
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      21-Feb-2013 16:37:08 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;
use XML::Simple;

my $dir = $ARGV[0];
chomp $dir;
opendir(DIR, $dir) or die $!;

my $motifstotest = $ARGV[1];
my $findsize = $ARGV[2] || 150;

#my $resultsdir = "/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/3_homer_find/";
#my $peakhomerdir = "/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/1_repeat_masker/homer100/";
#my $logfile = "/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/batchhomerfind.log";
my $resultsdir = "/home/fn231/pacific/Felicia/ANALYSIS_1/motif_analysis_cell_specific_regions/all_tomtom_results_4/subset/homer_find/";
my $peakhomerdir = "/home/fn231/pacific/Felicia/ANALYSIS_1/motif_analysis_cell_specific_regions/bed100_common_hpc7_mast/";
my $logfile = "/home/fn231/pacific/Felicia/ANALYSIS_1/motif_analysis_cell_specific_regions/all_tomtom_results_4/subset/homer_find/batchhomerfind.log";
open( LOG, ">$logfile" ) or die "Can't open $logfile !!";

#my $jaspardir = "/home/fn231/pacific/Felicia/RESOURCES/db/Jaspar/trim_IC0.5_4bp/homer_format/LLR_logIC_thr_1/";
#my $genome = "mm10";
my $jaspardir = "/home/fn231/pacific/Felicia/ANALYSIS_1/motif_analysis_cell_specific_regions/all_tomtom_results_4/subset/homer_motifs/";
my $genome = "mm9";
my $idx = 1;

my $pwmhashref = loadPWMInfo();

while (my $xmlfile = readdir(DIR)) {
	
	my $jcount = 0;
	
	# Ignore files beginning with a period
	next if ($xmlfile =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$xmlfile");
	# Ignore non XML files
	next if ($xmlfile !~ m/.xml$/);
	
	print LOG "($idx) Runnning HOMER for $xmlfile\n";
	
	# Get motif ID for all TOMTOM matches
	my $motifidref = getMotifId($xmlfile);

	# Generate homer-formatted peak file name for each sample
	$xmlfile =~ s/.xml//;
	my $homerfile = $peakhomerdir . $xmlfile . ".homer.txt";
	unless(-e $homerfile) {
		die "Peak file not found: $xmlfile.homer.txt\n";
	}
		
	# Run HOMER (findMotifsGenome.pl -find) for each motif match
	# Only test against matches to Jaspar motifs
	foreach my $motif (@{$motifidref}) {
		
		# Check if ID exists ($collection is defined)
		next unless defined $pwmhashref->{$motif}->{'collection'};
		my $collection =  $pwmhashref->{$motif}->{'collection'};
		chomp $collection;
			
		print LOG "\tTesting motif: $motif\n";
			
		# Create 1 output directory for each XML file
		my $outdir = $resultsdir . $xmlfile;
		unless(-e $outdir or mkdir $outdir) {
			die "Unable to create directory: $xmlfile\n";
		}
			
		my $motiffile = $jaspardir . $motif . ".motif";
		unless(-e $motiffile) {
			die "Motif file not found: $motiffile\n";
		}
		my $outfile = $outdir . "/" . $motif . ".find.txt";
		
		my $homercmd = "findMotifsGenome.pl $homerfile $genome $outdir -size $findsize -find $motiffile > $outfile";
		system $homercmd;

		# Remove motifFindingParameters.txt
		unlink("$outdir/motifFindingParameters.txt") or warn "Could not unlink: motifFindingParameters.txt\n";
			
		$jcount++;

	}
	
	if ($jcount==0) {
		print LOG "\tNo matches to list of tested motifs!\n";	
	}	
	$idx++;

}

sub getMotifId {
	
	my $filename = $_[0];
	my @ids;

	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($filename);
	  
	my %tfile =  %{$doc->{targets}->{target_file}};
	
	if (exists($tfile{'motif'})) {
			
			my $t = $tfile{'motif'};
			my @motifids = keys %{$t};
			
			if ($motifids[0] =~ m/^[A-Z]+/) {	# Motif IDs as keys
				foreach my $motifid (@motifids) {
					push(@ids, $motifid);
				}
			}else {
				push(@ids, $t->{'name'});		# attributes as keys
			}	
			
	}
	
	return(\@ids);
	
}

sub loadPWMInfo {
	# Store jaspar motif information in a hash
	my %pwmhash = ();
	
	my $pwmfile = $motifstotest;	# list of motifs to test
	unless( open(FILE, $pwmfile) ) {
       	print STDERR "Cannot open file \"$pwmfile\"\n\n";
       	exit;
    }
    my @jaspardata = <FILE>;
	close FILE;
	    
	foreach my $line (@jaspardata) {
		my @element = split ( '\t', $line );
		my $id = $element[0];
		my $name = $element[1];
		my $coll = $element[2];
		$pwmhash{$id}{'name'} = $name;
		$pwmhash{$id}{'collection'} = $coll;
	}
	return \%pwmhash;
}