#!/bin/bash

outdir="/home/fn231/results/gottgens/felicia/COMPENDIUM/pipeline_mm10_reprocessed/peak_len"

for file in *.bed; 
do	
	awk '{print $3-$2}' $file | sort  > "$outdir/${file/.bed}.txt"
done

cd $outdir

wc *.txt > summary_len.txt