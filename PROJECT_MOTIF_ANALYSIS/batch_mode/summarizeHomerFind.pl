#!/usr/bin/perl
#===============================================================================
#
#  FILE:        summarizeHomerFind.pl
#
#  USAGE:       ./summarizeHomerFind.pl [path_to_results]
#
#  DESCRIPTION: This script reads all HOMER findMotifsGenome.pl -find results 
#				(*.find.txt) and summarizes results into a super matrix with 
#				coordinates in rows and motifs in columns.
#
# OPTIONS:      [path_to_results]  path to results files
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      27-Feb-2013 17:36:06 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;

my $homerFindDir = $ARGV[0];
opendir DIR, $homerFindDir or die "Couldn't open HOMER -find results dir '$homerFindDir': $!";
my @dirlist = readdir(DIR); 
closedir DIR;

my $peakfiledir = "/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/1_repeat_masker/homer100/";
my $outdir = "/home/fn231/pacific/Felicia/COMPENDIUM/pipeline_mm10/test/homer_find_merged/";

# Iterate over all results directory (1 for every peak file)
foreach my $dir (@dirlist) {
	
	# Ignore files beginning with a period
	next if ($dir =~ m/^\./);
	# Check if $dir is a directory
	next unless (-d "$dir");
	
	# Open the results directory
	my $path = $homerFindDir . "/" . $dir;
	opendir DIR, $path or die "\tCouldn't open dir '$path': $!";
	my @resultsfiles = readdir(DIR);
	closedir DIR;

	# Load peak file data and store in a hash
	my $peakfile = $peakfiledir . $dir . ".homer.txt";
	my $peakhashref = loadPeakFile($peakfile);
	
	# Iterate over all results file
	foreach my $findfile (@resultsfiles) {
		
		# Ignore non-result files (results file end with ".find.txt")
		next if ($findfile !~ m/.find.txt$/);

    	my @finddata = get_file_data($path . "/" . $findfile);
		
		# Ignore files with no results
		next if ( (scalar @finddata) == 1 );
		
		# Remove header line
		shift(@finddata);
		
		$findfile =~ s/.find.txt//;
		my $outfile = $outdir . $findfile . ".bed";
		open( OUT, ">>$outfile" ) or die "Can't open output file: $outfile !!";

		foreach my $line (@finddata) {
			
			(my $id, my $offset, my $seq, my $motif, my $strand, my $score) = split ( '\t', $line);
			my $seqlen = length ($seq);
			
			my $motifchr;
			my $motifstart;
			my $motifend;
			my $peakctr;
			
			$peakctr = ( ($peakhashref->{$id}->{'end'} - $peakhashref->{$id}->{'start'}) / 2 ) + $peakhashref->{$id}->{'start'};
			
			if ($strand eq "+") {	
				$motifchr = $peakhashref->{$id}->{'chr'};
#				$motifstart = $peakctr - $offset - $seqlen + 1;
#				$motifend = $peakctr - $offset;
				$motifstart = $peakctr + $offset;
				$motifend = $peakctr + $offset + $seqlen - 1;
			}elsif ($strand eq "-") {
				$motifchr = $peakhashref->{$id}->{'chr'};
#				$motifstart = $peakctr - $offset;
#				$motifend = $peakctr - $offset + $seqlen - 1;
				$motifstart = $peakctr + $offset -$seqlen + 1;
				$motifend = $peakctr + $offset;
			}
			
			chomp $motifchr;
			chomp $motifstart;
			chomp $motifend;
			
#			my $peakchr = $peakhashref->{$id}->{'chr'};
#			my $peakstart = $peakhashref->{$id}->{'start'};
#			my $peakend = $peakhashref->{$id}->{'end'};
			
			#print OUT $peakchr, "\t", $peakstart, "\t", $peakend, "\tid=", $id, "\toffset=", $offset, "\tlen=", $seqlen, "\t";
			print OUT $motifchr, "\t", $motifstart, "\t", $motifend, "\t", $strand, "\n";
		}
		
	}
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

sub loadPeakFile {

	my $filename = $_[0];
	my %hash = ();
	
	my @data = get_file_data($filename);
	    
	foreach my $line (@data) {
		(my $id, my $chr, my $start, my $end) = split ( '\t', $line );
		$hash{$id}{'chr'} = $chr;
		$hash{$id}{'start'} = $start;
		$hash{$id}{'end'} = $end;
	}
	
	return \%hash;
	
}