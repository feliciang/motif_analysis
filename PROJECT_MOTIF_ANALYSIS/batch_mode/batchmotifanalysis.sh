#!/bin/bash
#===============================================================================
#
#  FILE:        batchmotifanalysis.sh
#
#  USAGE:       ./batchmotifanalysis.sh [genome]
#
#  DESCRIPTION: Script to run HOMER and TOMTOM analysis on all peak files in the
#				compendium  
#				
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION:	Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      04-Feb-2013
#===============================================================================

genome=$1
path=${2:-"$WORKDIR/COMPENDIUM/pipeline_mm10_reprocessed"}

currdir=$PWD
resultsdir="$path/2_homer_tomtom"
xmldir="$path/2_tomtom_xml"
targetthr=5
pthr=1e-10
idx=1

for file in *.homer.txt
do

	logFile="${file/.homer.txt}.log"
	
	# Run HOMER
	if [ ! -d "$resultsdir/${file/.homer.txt}" ]; then
    	mkdir "$resultsdir/${file/.homer.txt}"	# Folder named after peakfile
	fi
	echo "($idx) Running HOMER for $file"
	homerdir="${resultsdir}/${file/.homer.txt}"
	findMotifsGenome.pl $file $genome "$homerdir/" -size given -noknown &> "$resultsdir/$logFile"
	cd $homerdir
	
    # Remove "knownResults" and "geneOntology" lines from HTML file
	grep -v "knownResults.html\|geneOntology.html" homerResults.html > homerResults2.html
	rm homerResults.html
	mv homerResults2.html homerResults.html
	
	# Convert homer output to MEME format
    perl "$REPOS/motif_analysis/PROJECT_MOTIF_ANALYSIS/formating/homer2meme.pl" ./homerResults/ $targetthr $pthr > ./homerResults.meme.txt

	# Run TOMTOM if homerResults.meme.txt contains motif PWMs (skips this step if empty)
    nlines=`wc homerResults.meme.txt | awk '{print $1}'`
    
    if [ "$nlines" -gt 9 ]; then
	    echo "($idx) Running TOMTOM"
	    tomtom -o tomtom -thresh 0.05 homerResults.meme.txt "$RESOURCES/db/motif_db_merged/Jaspar_Uniprobe_Jolma.meme.txt" &>> "$resultsdir/$logFile"
	    echo "Finished!"
	    
	    # Copy TOMTOM xml file to common folder
		tomtomfile="${file/.homer.txt}.xml"
		cp "$homerdir/tomtom/tomtom.xml" "$xmldir/$tomtomfile"
	else
		echo "($idx) WARNING: No motifs found. Skipping TOMTOM!"
		rm homerResults.meme.txt
	fi
	
    # Jump back to compendium directory
	cd $currdir
	idx=`expr $idx + 1`
	
done