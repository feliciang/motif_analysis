#!/bin/bash

# *.dist.bed file
distFile=$1
fileExt=$2

# Delete existing mpcoord.bed file
if [ -f mpcoord.txt ]; then
	rm mpcoord.txt
fi

# Split *.dist.bed file based on offset value
awk '$10>0 {print $0}' $distFile > "${distFile/.dist.bed}.posdist.bed"
awk '$10<0 {print $0}' $distFile > "${distFile/.dist.bed}.negdist.bed"

# Get start and end of motif pair coordinates
awk 'BEGIN{OFS="\t"} {print $1,$2,$7}' "${distFile/.dist.bed}.posdist.bed" > "${distFile/.dist.bed}.$fileExt"
awk 'BEGIN{OFS="\t"} {print $5,$6,$3}' "${distFile/.dist.bed}.negdist.bed" >> "${distFile/.dist.bed}.$fileExt"

# Delete temporary files
rm "${distFile/.dist.bed}.posdist.bed" "${distFile/.dist.bed}.negdist.bed"