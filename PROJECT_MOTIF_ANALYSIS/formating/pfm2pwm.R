#===============================================================================
#
#  FUNCTION:    pfm2pwm
#
#  DESCRIPTION: Converts frequency matrix to weight matrix
#				
#  ARGUMENTS:
#  mat:
#	 matrix of alphabet (A,C,G,T) frequency in jaspar .pfm format.  Matrix must
#	 have 4 rows.
#
#  RETURN:  	weight matrix
#
#  AUTHOR:      Felicia Ng (fn231)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     3 Jul 2013
#===============================================================================

pfm2pwm <- function(mat) {
	
	colsum <- colSums(mat)
	pwm <- signif(t(t(mat)/colsum), digits=6)
	return(pwm)
	
}