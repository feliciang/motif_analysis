#!/usr/bin/perl
#===============================================================================
#
#  FILE:        homerPreparsedSeq2fasta.pl
#
#  USAGE:       ./homerPreparsedSeq2fasta.pl [path]
#
#  DESCRIPTION: This script takes in a *.seq file (HOMER preparsed genome data)
#				and converts it to fasta format
#
# OPTIONS:      [path]  path to *.seq file
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      4-Feb-2014 14:08:06 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;

my $homerSeqFile = $ARGV[0];
my @homerSeqData = get_file_data($homerSeqFile);

my $outFile=$ARGV[1];
open(OUT, ">$outFile");

foreach my $line (@homerSeqData) {
	
	my @elements = split( "\t", $line);
	print OUT ">", $elements[0], "\n";
	print OUT $elements[1];
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}