#!/usr/bin/perl -w
#===============================================================================
#
#  FILE:        jolma2meme.pl
#
#  USAGE:       ./jolma2meme.pl
#
#  DESCRIPTION: Script to convert jolma txt file to MEME format
#
# OPTIONS:      options
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      04-Feb-2013 14:58:25 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;
use myutils;

my $jolmafile = $ARGV[0];
my @data = get_file_data($jolmafile);
my $id = 0;

open MATOUT, ">matrix_list.txt";
print MATOUT "symbol\tfamily\tclone type\tligand sequence\tbatch\tSeed\tmultinomial\tcycle\tsite type\tcomment\tMatrix is  one of the representative PWMs\n";

foreach my $line (@data) {
	
	my $name;
	my $fname = "Jolma.".$id.".pfm";
	
	if ($line =~ m/^[A-Za-z0-9-]{2,}\t/) {
		$id++;
		$name = "Jolma.".$id;
		chomp $line;
		print MATOUT $name, "\t", $line, "\n";
	}elsif ($line =~ m/^[ACGT]{1}\t/) {
		my @element = split ("\t", $line);
		open PFMOUT, ">>$fname";
		my $len = scalar(@element) - 1;
		for (my $i=1; $i<=$len; $i++) {
			chomp($element[$i]);
			print PFMOUT $element[$i], "\t";
		}
		print PFMOUT "\n";
	}else {
		print "ERROR: ", $line, "\n";
	}

}