#!/usr/bin/perl
# 
# Script to read in *.motif files (HOMER output) and convert to MEME format 
# (*similar*.motif and *RV.motif files are ignored)
#
# Felicia Ng
# 16 Nov 2012

use strict;
use warnings;

my $dir = $ARGV[0];
chomp $dir;
opendir(DIR, $dir) or die $!;

my $targetthr = $ARGV[1] || 0; 	# default % Target is zero (i.e. use all motifs)
my $pthr = $ARGV [2] || "1e-10";		# default p-value is 1e-10

printHeader();

while (my $file = readdir(DIR)) {
	
	# Ignore files beginning with a period
	next if ($file =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$file");

	if ($file =~ m/similar/) {	# Ignore files that have 'similar' in the file name
		next;
	}elsif ($file =~ m/RV/) {	# Ignore motif reverse complement (tomtom matches rev comp of all query motifs)
		next;
	}elsif ($file =~ m/.motif$/) {		
		# Obtain file data
		my $filepath = $dir . $file;
		my @data = get_file_data($filepath);
		my $width = scalar(@data) - 1;
		
		# Get motif name
		$file =~ m/^motif([0-9]+).motif/;
		my $motifid = $1;
		
		# Parse annotation line (begins with ">")
		my @elements = split ("\t", $data[0]);
		my @elements2 = split (",", $elements[5]);		# Example:  T:14.0(4.71%),B:12.7(0.03%),P:1e-26
		
		# Get "Target" information
		$elements2[0] =~ m/^T:(\d+\.\d+)(\(\d+\.\d+%\))$/;
		my $target = $1;
		my $targetpct = $2;
		$targetpct =~ s/[\(\)%]//g;
		
		# Get "p-value" information
		$elements2[2] =~ m/^P:(\d+e-\d+)$/;
		my $pvalue = $1;
		
		if ($targetpct >= $targetthr && $pvalue <= $pthr ) {
			
			print "\nMOTIF ", $motifid, "\n\n";
			
			foreach my $line (@data) {
				if ($line =~ m/^>/) {
						print "letter-probability matrix: alength= 4 w= ", $width, " nsites= ", $target, " E= 0\n";	
				}elsif ($line =~ m/^(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/) {
					printf "  %.6f\t  %.6f\t  %.6f\t  %.6f\n", $1, $2, $3, $4;			
				}else {
					next;
				}
			}
		}	
	}else {
		next;
	}
		
}

sub printHeader {
	
	# Print header for meme output file
	print "MEME version 4.4\n\n";
	print "ALPHABET= ACGT\n\n";
	print "strands: + -\n\n";
	print "Background letter frequencies (from uniform background):\n";
	print "A 0.25000 C 0.25000 G 0.25000 T 0.25000\n\n";

}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}