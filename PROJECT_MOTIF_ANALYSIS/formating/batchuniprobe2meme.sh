#!/bin/bash
#
# Script to convert all Uniprobe pwm files in a folder to meme format 
#
# Felicia Ng 
# 20 Nov 2012

for i in *.pwm
do
	outname=${i/.pwm}_meme.txt
	uniprobe2meme $i > $outname
done