#!/usr/bin/perl

use strict;
use warnings;
#use myutils;
use Bio::Matrix::PSM::SiteMatrix;

my $file = $ARGV[0];
my @data = get_file_data($file);
my $ct = 0;
my $width = 0;
my @a = ();
my @c = ();
my @g = ();
my @t = ();

foreach my $line (@data) {

	my $motifname;
	
	if ($line =~ m/^MOTIF\s+([A-Za-z0-9._-]+)\s+[A-Za-z0-9._-]+/) {
		$motifname = $1;
		print $motifname, "\n";
	}elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= (\d+)/) {
		$width = int($1);
	}elsif ($line =~ m/^\s{2}(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+/) {
		push(@a, $1);
		push(@c, $2);
		push(@g, $3);
		push(@t, $4);
		$ct++;
		
		if ($width==$ct) {
			my $iupac = getIUPAC(\@a, \@c, \@g, \@t);
			print $iupac, "\n";
			$ct = 0;
			@a = ();
			@c = ();
			@g = ();
			@t = ();
		}
	}else {
		next;
	}

}

sub getIUPAC {
	
	my ($a, $c, $g, $t, $mid) = @_;
	
	my %param=(-pA=>$a,-pC=>$c,-pG=>$g,-pT=>$t,-id=>$mid);
	my $site=Bio::Matrix::PSM::SiteMatrix->new(%param);
	
	my $iupac=$site->IUPAC;
	return $iupac;
	
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;

}

exit;