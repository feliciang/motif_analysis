#! /usr/bin/perl -w

# ** Modified from Rebecca's script **
# Perl script to convert reads in .bed files to desired length.

# If on the reverse strand (-), take away length from the end number and replace start with new number e.g.
# 	3'	5'
#	<--------
#	S	E

# If on the forward strand (+), add length to start and use new number as end

use strict;

my $extend = "";

open IN, "$ARGV[0]" || die "Can't open input file!";
open OUT, ">$ARGV[1]" || die "Can't open output file!";

# 1. chr	start	end	strand
# 2. chr	start	strand
# 3. chr	start	end

my $selec = 3;
chomp $selec;

my $length = $ARGV[2];
chomp $length;

my $strand_choice = "n";
chomp $strand_choice;

$extend = "c";
chomp $extend;

if ($strand_choice eq "y")
{

if ($extend eq "c")
{

	while(<IN>)
	{
	chomp $_;
	my ($chr, $start, $end, $FR) = split /\t/;
	chomp $start;
	chomp $end;
	chomp $chr;	
	chomp $FR;
	my $summit = (($end - $start)/2)+$start;
		if ($summit =~ m/(\d+)\.\d+/)
		{
		$summit = $1;
		}
	my $half_length = $length/2;
	my $new_end = $summit + $half_length;
	my $new_start = $summit - $half_length;
	if ($new_start < 0)
	{
	$new_start = 0;
	$new_end = 0 + $length;
	}
	print OUT "$chr\t$new_start\t$new_end\t$FR\n";
				
	}#close while

}#close extend if c
elsif ($extend eq "s")
{

	if ($selec == 1)
	{
		#print "\nWould you like to convert all to forward strand? (y/n):\n";
		my $choice = "n";
		chomp $choice;
	
		if($choice eq "y")
		{
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $end, $FR) = split /\t/;
		my ($new_end, $new_start) = (0,0);
		chomp ($chr, $FR, $start, $end);
			if ($FR =~ /^\+/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^F/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			else
			{
			$new_end = $end;
			$new_start = $end - $length;
			}
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\t$FR\n";
				
		}#close while		

		}#close if
		else
		{
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $end, $FR) = split /\t/;
		my ($new_end, $new_start) = (0,0);
		chomp ($chr, $FR, $start, $end);
			if ($FR =~ /^\+/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^F/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			else
			{
			$new_end = $end;
			$new_start = $end - $length;
			}
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\t$FR\n";
				
		}#close while	
		}

	}#close if selec 1

	elsif ($selec == 2)
	{

		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $FR) = split /\t/;
		my ($new_end, $new_start) = (0,0);
		#print "\$chr = $chr, \$start = $start, \$FR = $FR\n";
		chomp $chr;	
		chomp $FR;
		chomp $start;
			if ($FR =~ /^\+/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^F/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^\-/)
			{
			$new_start = $start - $length;
			$new_end = $start;
			}
			elsif ($FR =~ /^R/)
			{
			$new_start = $start;
			$new_end = $start - $length;
			}
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\t$FR\n";
		}#close while

	}
	elsif ($selec == 3)
	{
	
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $end, $rest) = split /\t/;
		chomp $chr;
		chomp $start;
		my $new_start = $start;
		my $new_end = $start + $length;
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\n";
		}

	}#close selec if 3

}#close elsif extend s

}#close if strand y

elsif ($strand_choice eq "n")
{

if ($extend eq "c")
{

	if ($selec == 2)
	{
	while(<IN>)
	{
	chomp $_;
	my ($chr, $start, $FR) = split /\t/;
	my ($new_end, $new_start) = (0,0);
	chomp $chr;
	chomp $start;
	my $half_length = $length/2;
	$new_end = $start + $half_length;
	$new_start = $start - $half_length;
		if ($new_start =~ m/^\-/)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
	print OUT "$chr\t$new_start\t$new_end\n";
	
	}#close while
	}
	else
	{
	while(<IN>)
	{
	chomp $_;
	my ($chr, $start, $end, $rest) = split /\t/;
	my ($new_end, $new_start) = (0,0);
	chomp $chr;
	chomp $start;
	chomp $end;
	my $summit = (($end - $start)/2)+$start;
		if ($summit =~ m/(\d+)\.\d+/)
		{
		$summit = $1;
		}
	my $half_length = $length/2;
	$new_end = $summit + $half_length;
	$new_start = $summit - $half_length;
		if ($new_start =~ m/^\-/)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
	print OUT "$chr\t$new_start\t$new_end\n";
	
	}#close while
	}#close else
}
elsif ($extend eq "s")
{
	if ($selec == 1)
	{
		#print "\nWould you like to convert all to forward strand? (y/n):\n";
		my $choice = "n";
		chomp $choice;
	
		if($choice eq "y")
		{
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $end, $FR) = split /\t/;
		my ($new_end, $new_start) = (0,0);
		chomp ($chr, $FR, $start, $end);
			if ($FR =~ /^\+/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^F/)
			{
			$new_start = $start;
			$new_end = $start + $length;
			}
			else
			{
			$new_end = $end;
			$new_start = $end - $length;
			}
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\n";
				
		}#close while		

		}#close if
		else
		{
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $end, $FR) = split /\t/;
		my $new_end = "";
		my $new_start = "";
		chomp $chr;	
		chomp $FR;
			if ($FR =~ /^\+/)
			{
			chomp $start;
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^F/)
			{
			chomp $start;
			$new_start = $start;
			$new_end = $start + $length;
			}
			elsif ($FR =~ /^\-/)
			{
			chomp $end;
			$new_end = $end;
			$new_start = $end - $length;
			}
			elsif ($FR =~ /^R/)
			{
			chomp $end;
			$new_end = $end;
			$new_start = $end - $length;
			}
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\n";
		}#close while

		}#close else
	}
	elsif ($selec == 2)
	{
	
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $FR) = split /\t/;
		my ($new_end, $new_start) = (0,0);
		#print "\$chr = $chr, \$start = $start, \$FR = $FR\n";
		chomp $chr;	
		chomp $FR;
		chomp $start;
			if ($FR =~ /^\+/)
			{
			my $new_start = $start;
			my $new_end = $start + $length;
			}
			elsif ($FR =~ /^F/)
			{
			my $new_start = $start;
			my $new_end = $start + $length;
			}
			elsif ($FR =~ /^\-/)
			{
			my $new_end = $start;
			my $new_start = $start - $length;
			}
			elsif ($FR =~ /^R/)
			{
			my $new_end = $start;
			my $new_start = $start - $length;
			}
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\n";
		}#close while
		
	}
	elsif ($selec == 3)
	{
	
		while(<IN>)
		{
		chomp $_;
		my ($chr, $start, $end) = split /\t/;
		my ($new_end, $new_start) = (0,0);
		chomp $start;
		chomp $end;
		chomp $chr;
		$new_start = $start;
		$new_end = $start + $length;
		if ($new_start < 0)
		{
		$new_start = 0;
		$new_end = 0 + $length;
		}
		print OUT "$chr\t$new_start\t$new_end\n";
		}
	
	}

}#close elsif extend s

}#close elsif strand n

close IN;
close OUT;
