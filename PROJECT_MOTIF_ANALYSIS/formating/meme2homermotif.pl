#!/usr/bin/perl
#===============================================================================
#
#  FILE:        meme2homermotif.pl
#
#  USAGE:       ./meme2homermotif.pl motiffile
#
#  DESCRIPTION: Script to convert MEME motif files to HOMER .motif files
#
# OPTIONS:      motiffile	MEME format motif file
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      21-Feb-2013 14:14:51 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;

my $memefile = $ARGV[0];
my @data = get_file_data($memefile);

my $jasparid;
my $name;
my $homerid;
my $print = 0;

foreach my $line (@data) {	
	if ($line =~ m/^MOTIF\s+([A-Za-z0-9._-]+)\s+([A-Za-z0-9._-]+)/) {
		$jasparid = $1;
		$name = $2;
		$homerid = $1 . "_" . $2;
	}elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= (\d+) nsites= (\d+) E= (\d+)/) {
		$print = 1;
		my $outfile = $jasparid . ".motif";
		open(HOMER, ">$outfile");
		print HOMER ">$jasparid\t$homerid\t5\n";
	}elsif ($line =~ m/^\s{2}(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s+(\d.\d+)\s*/) {
		printf HOMER "%.6f\t  %.6f\t  %.6f\t  %.6f\n", $1, $2, $3, $4;			
	}elsif ($line =~ m/^\s{10,}/) {
		if ($print==1){
			$print = 0;
			close(HOMER);
		}
	}

}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}