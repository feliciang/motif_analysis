#!/usr/bin/perl -w
#===============================================================================
#
#  FILE:        bed2homer.pl
#
#  USAGE:       ./bed2homer.pl [input_file] [output_file]
#
#  DESCRIPTION: Convert bed file to HOMER format (id, chr, start, end, strand)
#
# OPTIONS:      [input_file] 	name of input file
#				[output_file]	name of output file
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      05-Feb-2013 13:33:42 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;

my $infile = shift;
my $outfile = shift;
my $option = shift;
my $i = 1;

my @indata = get_file_data($infile);
open(OUT, ">$outfile");

foreach my $line (@indata) {
	
	my $chr;
	my $start;
	my $end;
	my $strand = "+";
	
	my @elements = split ('\t', $line);
	
	if (scalar (@elements==3) || $option eq "ignorestrand" ) {
		($chr, $start, $end) = split ('\t', $line);
	}elsif (scalar (@elements==4) && $option eq "strand") {
		($chr, $start, $end, $strand) = split ('\t', $line);
	}
	
	chomp($chr);
	chomp($start);
	chomp($end);
	chomp($strand);
	
	print OUT $i, "\t", $chr, "\t", $start, "\t", $end, "\t", $strand, "\n";
	$i++;
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}