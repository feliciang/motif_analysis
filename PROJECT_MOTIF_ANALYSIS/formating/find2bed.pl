#!/usr/bin/perl
#===============================================================================
#
#  FILE:        find2bed.pl
#
#  USAGE:       ./find2bed.pl [path_to_results]
#
#  DESCRIPTION: This script reads all HOMER findMotifsGenome.pl -find results 
#				(*.find.txt) and summarizes results into a super matrix with 
#				coordinates in rows and motifs in columns. To be executed in
#				folder containing .find files.
#
# OPTIONS:      [queryfile]    path to file used in homer find input
#				[queryformat]  'bed' (0-based) or 'fasta'
#				[outdir]       path to output directory
# REQUIREMENTS: ---
# BUGS:         ---
# NOTES:        ---
# AUTHOR:       Felicia Ng (fn231)
# ORGANIZATION: Cambridge Institute for Medical Research
# VERSION:      1.0
# CREATED:      27-Feb-2013 17:36:06 GMT
# REVISION:     ---
#===============================================================================

use strict;
use warnings;
use Time::Local;

#my $queryfile = "$ENV{'RESOURCES'}/compendium/mm10/samples_289/repeat_removed/all_peaks.homer.txt";
my $queryfile = $ARGV[0];
my $queryformat = $ARGV[1];
#my $outdir = "$ENV{WORKDIR}/ANALYSIS_2/1_motif_pair/7_analysis/2_scan_HAEMCODE_regions/motifs/find2bed";
my $outdir = $ARGV[2];

# Make sure output directory ends with forward slash
if ($outdir !~ /\/$/) {
	$outdir .= "/";
}

# Get all .find file names, die if none exists
opendir DIR, $ENV{'PWD'} or die "Couldn't open HOMER -find results dir '$ENV{'PWD'}': $!";
my @resultsfiles = readdir(DIR);
closedir DIR;
if (scalar(@resultsfiles) < 1) {
	die("No .find.txt files found!");
}

# Load peak file data and store in a hash
my $queryhashref = loadQueryFile($queryfile, $queryformat);

# Iterate over all results file
foreach my $findfile (@resultsfiles) {
		
	# Ignore non-result files (results file end with ".find.txt")
	next if ($findfile !~ m/.find.txt$/);
   	my @finddata = get_file_data($findfile);
	
	# Ignore files with no results
	next if ( (scalar @finddata) == 1 );
	
	# Remove header line
	shift(@finddata);
	
	#Prepare output file (delete file if an old one exists)
	$findfile =~ s/.find.txt//;
	my $outfile = $outdir . $findfile . ".bed";
	if (-e $outfile) {
 		my $cmd="rm $outfile";
 		system($cmd);
 	}
	open( OUT, ">>$outfile" ) or die "Can't open output file: $outfile !!";

	foreach my $line (@finddata) {
			
		(my $id, my $offset, my $seq, my $motif, my $strand, my $score) = split ( '\t', $line);
		$id =~ s/^\s+|\s+$//g;
		
		my $seqlen = length ($seq);
			
		my $motifchr;
		my $motifstart;
		my $motifend;
		my $peakctr;
		
		$peakctr = ( ($queryhashref->{$id}->{'end'} - $queryhashref->{$id}->{'start'}) / 2 ) + $queryhashref->{$id}->{'start'};

		if ($strand eq "+") {	
			$motifchr = $queryhashref->{$id}->{'chr'};
			$motifstart = $peakctr + $offset;
			$motifend = $peakctr + $offset + $seqlen - 1;
		}elsif ($strand eq "-") {
			$motifchr = $queryhashref->{$id}->{'chr'};
			$motifstart = $peakctr + $offset -$seqlen + 1;
			$motifend = $peakctr + $offset;
		}
			
		chomp $motifchr;
		chomp $motifstart;
		chomp $motifend;
			
		print OUT $motifchr, "\t", $motifstart, "\t", $motifend, "\t", $strand, "\n";
		
	}
		
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

sub loadQueryFile {

	my ($filename, $format) = @_;
	my %hash = ();
	
	my @data = get_file_data($filename);
	    
	if ($format eq "bed") {
		foreach my $line (@data) {
			(my $id, my $chr, my $start, my $end, my $strand) = split ( '\t', $line );
			$hash{$id}{'chr'} = $chr;
			$hash{$id}{'start'} = $start;
			$hash{$id}{'end'} = $end;
		}
	}elsif ($format eq "fasta") {
		my $id;
		foreach my $line (@data) {
			if ($line =~ /^>/) {
				( $id = $line ) =~ s/>\s*//g;
				$id =~ s/^\s+|\s+$//g;
				$hash{$id}{'chr'} = $id;
				$hash{$id}{'start'} = 1;  
			}elsif ($line =~ /^[ACGT]+$/) {
				chomp $line;
				$hash{$id}{'end'} = length($line) + 1; # add 1 because fasta is in 1-based format
				undef $id;
			}
		}
	}
	
	return \%hash;
	
}