#!/usr/bin/perl

use strict;
use warnings;
use XML::Simple;
use Data::Dumper;

my $dir = $ARGV[0];
opendir(DIR, $dir) or die $!;
my $mode = $ARGV[1];
my %results = ();
my @allids;
my @allfiles;
my @allsmpls;

# Go through all files in $dir and obtain all motif id names
while (my $file = readdir(DIR)) {

	# Ignore non XML files
	next if ($file !~ m/.xml$/);
	# Ignore files beginning with a period
	next if ($file =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$file");
	
	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($file);
	my %qfile =  %{$doc->{queries}->{query_file}};
	my $q = $qfile{'query'};
	
	if (ref($q) eq "HASH" && exists($q->{'match'}) ) {		# For instances where there is only 1 query motif
		my $id = getID($q);
		push(@allids, $id);
	}elsif (ref($q) eq "ARRAY") {							# For instances where there is more than 1 query motif
		foreach my $qref (@$q) {
			if ( exists($qref->{'match'}) ) {
				my $id = getID($qref);
				push(@allids, $id);
			}	
		}		
	}	

	# Store file names in an array
	push(@allfiles, $file);
	
	# Remove file extension and use as sample name
	my @fname = split ( /\./, $file );
	my $fileext = "." . pop(@fname);
	(my $smpl = $file) =~ s/$fileext//g;
	push(@allsmpls, $smpl);
	
}

# Create hash table of all motifs/samples
@allids = sort(@allids);
@allsmpls = sort(@allsmpls);
foreach my $id (@allids) {
	my $initval;
	if ($mode eq "binary") {
		$initval = 0;
	}elsif ($mode eq "qvalue") {
		$initval = 'NA';
	}	 
	%{$results{$id}} = map { $_ => $initval } @allsmpls;
}

# Go through all files in $dir again and obtain motif e-values
foreach my $file (@allfiles) {
	
	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($file);
	my %qfile =  %{$doc->{queries}->{query_file}};
	my $q = $qfile{'query'};
	
	# Remove file extension and use as sample name
	my @fname = split ( /\./, $file );
	my $fileext = "." . pop(@fname);
	(my $smpl = $file) =~ s/$fileext//g;
	
	if (ref($q) eq "HASH" && exists($q->{'match'}) ) {		# For instances where there is only 1 query motif
		my ($id, $qval) = getQvalue($q, $mode);
		$results{$id}{$smpl} = $qval;
	}elsif (ref($q) eq "ARRAY") {							# For instances where there is more than 1 query motif
		foreach my $qref (@$q) {
			if ( exists($qref->{'match'}) ) {
				my ($id, $qval) = getQvalue($qref, $mode);
				$results{$id}{$smpl} = $qval;				
			}
		}		
	}

}

#print Dumper \%results;
closedir(DIR);

# Print results stored in hash
my @motiftmp = sort keys %results;
my @samplestmp = sort keys %{ $results{$motiftmp[0]} };
print "motif";
foreach my $sample (@samplestmp) {
	print "\t", $sample;
}
print "\n";

foreach my $motif ( sort keys %results ) {
    print $motif;
    foreach my $sample ( sort keys %{ $results{$motif} } ) {
        	print "\t$results{$motif}{$sample}";
    }
    print "\n";
}

exit;

sub getID {
	
	my ($queryref) = @_;
	my $matchid;
		
	# Get all motif instances where there IS match in Jaspar
	if ( ref($queryref->{'match'}) eq "HASH" ) {
		$queryref->{'match'}->{'target'} =~ s/t_[0-9]+_//;
		$matchid = $queryref->{'match'}->{'target'};
	}elsif ( ref($queryref->{'match'}) eq "ARRAY" ) {
		my @tmp = @{$queryref->{'match'}}; 
		$tmp[0]->{'target'} =~ s/t_[0-9]+_//;
		$matchid = $tmp[0]->{'target'};
	}
	
	return $matchid;
	
}

sub getQvalue {
	
	my ($queryref, $mode) = @_;
	my $matchid;
	my $qvalue;
		
	# Get all motif instances where there IS match in Jaspar
	if ( ref($queryref->{'match'}) eq "HASH" ) {
		$queryref->{'match'}->{'target'} =~ s/t_[0-9]+_//;
		$matchid = $queryref->{'match'}->{'target'};
		if ($mode eq "binary") {
			$qvalue = 1;		
		}elsif ($mode eq "qvalue") {
			$qvalue = $queryref->{'match'}->{'qvalue'};
		}
	}elsif ( ref($queryref->{'match'}) eq "ARRAY" ) {
		my @tmp = @{$queryref->{'match'}}; 
		$tmp[0]->{'target'} =~ s/t_[0-9]+_//;
		$matchid = $tmp[0]->{'target'};
		if ($mode eq "binary") {
			$qvalue = 1;		
		}elsif ($mode eq "qvalue") {
			$qvalue = $tmp[0]->{'qvalue'};
		}
	}
	
	return ($matchid, $qvalue);
	
}