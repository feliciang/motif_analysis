#!/usr/bin/perl

use strict;
use warnings;
use XML::Simple;
use Data::Dumper;

my $file = $ARGV[0];

# create object
my $xml = new XML::Simple;

# read XML file
my $data = $xml->XMLin($file);

# print output
print Dumper($data);