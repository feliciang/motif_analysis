#!/usr/bin/perl
#===============================================================================
#
#  FILE:         scanFastaForKnownMotifs.pl
#
#  USAGE:        ./scanFastaForKnownMotifs.pl [fasta]
#
#  DESCRIPTION:  description
#
#  OPTIONS:      [fasta] Input fasta file containing sequences to be scanned
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      04-Feb-2014 14:12:30 GMT
#===============================================================================

use strict;
use warnings;

my $fasta = $ARGV[0];
my $outdir = $ARGV[1];
my $motifstotest = $ARGV[2];
my $pwmhashref = loadPWMInfo();
my $motifdir = "/home/fn231/pacific/Felicia/RESOURCES/db/Jaspar_wo_uniprobe/trim_IC0.5_2bp/homer_format/LLR_logIC_thr_0.5/";

for my $motifid ( sort keys %$pwmhashref ) {
    print "$motifid\t";
    
    my $motiffile = $motifdir . $motifid . ".motif";
    unless(-e $motiffile) {
		die "Motif file not found: $motiffile\n";
	}
	my $outfile = $outdir . "/" . $motifid . ".find.txt";
    
    my $homercmd = "findMotifs.pl $fasta fasta $outdir -find $motiffile > $outfile";
	system $homercmd;

	my @findData = get_file_data($outfile);
	
	# Delete files with no results
	if ( (scalar @findData) < 2 ) {
		unlink($outfile) or warn "Could not unlink: $outfile\n";
	}

	# Remove motifFindingParameters.txt
	unlink("$outdir/motifFindingParameters.txt") or warn "Could not unlink: motifFindingParameters.txt\n";
}
    
sub loadPWMInfo {
	# Store jaspar motif information in a hash
	my %pwmhash = ();
	
	my $pwmfile = $motifstotest;	# list of motifs to test
	unless( open(FILE, $pwmfile) ) {
       	print STDERR "Cannot open file \"$pwmfile\"\n\n";
       	exit;
    }
    my @jaspardata = <FILE>;
	close FILE;
	
	foreach my $line (@jaspardata) {
		my @element = split ( '\t', $line );
		my $id = $element[0];
		my $name = $element[1];
		my $coll = $element[2];
		$pwmhash{$id}{'name'} = $name;
		$pwmhash{$id}{'collection'} = $coll;
	}
	return \%pwmhash;
}

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
    
}