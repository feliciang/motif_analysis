#===============================================================================
#
#  FUNCTION:    calculateLLR
#
#  DESCRIPTION: For any given PFM, calculate the minimum Log Likelihood Ratio 
#				of a motif for use in findMotifsGenome.pl -find. Only calculated
#				for columns with information content of at least 'thr'. 
#				
#  ARGUMENTS:
#  thr:   
#    information content threshold
#  bg:
#	 vector of probabilities for the background model
#
#  RETURN:  	.motif file with adjusted log likelihood ratio	
#
#  AUTHOR:      Felicia Ng (fn231)
#  INST: 		Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     25 Feb 2013
#===============================================================================

calculateLLR <- function(thr=0.5, bg=c(0.25, 0.25, 0.25, 0.25)) {
	
	mfiles <- list.files(pattern=".motif")
	
	workPath <- getwd()
	if (file.exists(paste(workPath, "/LLR_logIC_thr_", thr, sep=""))==FALSE) { 
		dir.create(paste(workPath, "/LLR_logIC_thr_", thr, sep=""))
	}
	
	for (i in 1:length(mfiles)) {
		annot <- read.table(mfiles[i], sep="\t", nrows=1)
		pfm <- read.table(mfiles[i], sep="\t", skip=1)
		
		logIC <- apply(log2(pfm/bg)*pfm, 1, function(x) sum(na.omit(x)))
		
		# Get rows (nucleotide position) where information content is >= threshold
		idx <- which(logIC>=thr)
		maxP <- apply(pfm[idx,], 1, max)
		
		# Use log base e to calculate expected score (HOMER uses log base e)
		# HOMER subtracts a small value (0.01) from expected score
		# Equal distribution (0.25) of all nucleotides (AGCT)
		expScore <- sum(log(maxP/0.25, base=exp(1))) - 0.01
		if (expScore<5) {
			expScore <- 5
		}
		
		# Format matrix: 6 decimals
		pfm <- within(pfm, {
					V1 <- formatC(V1, format="f", digits=6)
					V2 <- formatC(V2, format="f", digits=6)
					V3 <- formatC(V3, format="f", digits=6)
					V4 <- formatC(V4, format="f", digits=6)
				})
		
		write.table(c(annot[1:2], expScore), file=paste(workPath, "/LLR_logIC_thr_", thr, "/", mfiles[i], sep=""), sep="\t", row.names=F, col.names=F, quote=F)
		write.table(pfm, file=paste(workPath, "/LLR_logIC_thr_", thr, "/", mfiles[i], sep=""), sep="\t", row.names=F, col.names=F, quote=F, append=T)
	}
	
}