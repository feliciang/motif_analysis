#!/usr/bin/perl
#===============================================================================
#
#  FILE:         filtermeme.pl
#
#  USAGE:        ./filtermeme.pl [meme_file] [motif_id_file] [mode]
#
#  DESCRIPTION:  Script to filter a MEME formated file. Program reads in a list
#				 of motif ids and filters the corresponding PWMs in the MEME
#				 file.  The motif is kept/discarded depending on the value of 
#				 the 'mode' parameter.
#
#  OPTIONS:      [meme_file] 		Name of MEME formatted file 
#				 [motif_names_file] Name of file containing motif
#				 [mode]		 		'tokeep' or 'todiscard' indicating whether 
#									the list of motif ids in the 'motif_id_file'
#									are to be kept or discarded.
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      16-Aug-2013 13:57:32 GMT
#===============================================================================

use strict;
use warnings;
use myutils;

# Read in meme file
my $file1 = $ARGV[0];
my @data1 = get_file_data($file1);

# Read in list of motif ids
my $file2 = $ARGV[1];
my @data2 = get_file_data($file2);
my %motifnames = map { chomp($_); $_ => 1 } @data2;

# Read in 'mode' of filtering
my $mode = $ARGV[2];

my $print = 0;

printHeader();

foreach my $line (@data1) {

	my $motif;
	
	if ($line =~ m/^MOTIF\s+([A-Za-z0-9._-]+)\s+[A-Za-z0-9._-]+/) {
		$motif = $1;
		if(!exists($motifnames{$motif}) && $mode eq "todiscard") {
			$print = 1;
		}elsif (exists($motifnames{$motif}) && $mode eq "tokeep") {
			$print = 1;
		}else {
			$print = 0;
		}
	}
#	elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= (\d+) nsites= (\d+) E= (\d+)/) {
#		$width = int($1);
#		$nsite = int($2);
#		$eval = float($3);
#	}

	if ($print==1) {
		print $line;
	}
}

sub printHeader {
	
	# Print header for meme output file
	print "MEME version 4.4\n\n";
	print "ALPHABET= ACGT\n\n";
	print "strands: + -\n";
	print "Background letter frequencies (from uniform background):\n";
	print "A 0.25000 C 0.25000 G 0.25000 T 0.25000\n\n";

}

exit;