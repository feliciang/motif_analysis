#!/usr/bin/perl
#===============================================================================
#
#  FILE:         getMotifNames.pl
#
#  USAGE:        ./getMotifNames.pl [meme_file]
#
#  DESCRIPTION:  Gets all motif IDs from a MEME file.
#
#  OPTIONS:      [meme_file]  Name of MEME formatted file
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      16-Aug-2013 15:17:00 GMT
#===============================================================================

use strict;
use warnings;
use Cwd;

# Read in MEME file
my $file = $ARGV[0];

# Obtain file data
my @data = get_file_data($file);

# Remove file extension
$file =~ s/.meme.txt//g;
	
my $outfile = $file. "_motif_names.txt";
open(OUT, ">$outfile");

foreach my $line (@data) {
	if ($line =~ m/^MOTIF/) {
		my @elements = split ( /\s+/, $line );
		print(OUT "$elements[1]\n");
	}else {
		next;
	}
}

close(OUT);

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

exit;