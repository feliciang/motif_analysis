#!/usr/bin/perl
#===============================================================================
#
#  FILE:         getAnnot.pl
#
#  USAGE:        ./getAnnot.pl [type]
#
#  DESCRIPTION:  Program to parse MEME files and extract information for each
#				 motif in the file.
#
#  OPTIONS:      [file_name] MEME file name 
#				 [type] type of information to retrieve (width, nsites, id, name)
#
#  AUTHOR:       Felicia Ng (fn231)
#  ORGANIZATION: Cambridge Institute for Medical Research
#  VERSION:      1.0
#  CREATED:      05-Aug-2013 12:51:27 GMT
#===============================================================================

use strict;
use warnings;
use Cwd;

my $file = $ARGV[0];
my $type = $ARGV[1];
my $id;

# Obtain file data
my @data = get_file_data($file);

# Remove file extension
my @fname = split ( /\./, $file );
my $fileext = "." . pop(@fname);
$file =~ s/$fileext//g;

if ($type eq "id") {
	my $outfile = $file. "_id.txt";
	open(OUT, ">$outfile");	
}elsif ($type eq "name") {
	my $outfile = $file. "_motif_names.txt";
	open(OUT, ">$outfile");
}elsif ($type eq "width") {
	my $outfile = $file. "_width.txt";
	open(OUT, ">$outfile");
}elsif ($type eq "nsites") {
	my $outfile = $file. "_nsites.txt";
	open(OUT, ">$outfile");
}

foreach my $line (@data) {
	if ($line =~ m/^MOTIF\s+([A-Za-z0-9._-]+)\s+[A-Za-z0-9._-]+/) {
		$id = $1;
		if ($type eq "id") {
			print(OUT "$1\n");	
		}
	}elsif ($line =~ m/^MOTIF\s+[A-Za-z0-9._-]+\s+([A-Za-z0-9._-]+)/ && $type eq "name") {
		print(OUT "$id\t$1\n");
	}elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= (\d+) nsites= \d+ E= \d+/ && $type eq "width") {
		print(OUT "$id\t$1\n");
	}elsif ($line =~ m/^letter-probability matrix: alength= \d+ w= \d+ nsites= (\d+) E= \d+/ && $type eq "nsites") {
		print(OUT "$id\t$1\n");
	}
}

close(OUT);

sub get_file_data {

    my($filename) = @_;

    # Initialize variables
    my @filedata = (  );

    unless( open(GET_FILE_DATA, $filename) ) {
        print STDERR "Cannot open file \"$filename\"\n\n";
        exit;
    }

    @filedata = <GET_FILE_DATA>;

    close GET_FILE_DATA;

    return @filedata;
}

exit;