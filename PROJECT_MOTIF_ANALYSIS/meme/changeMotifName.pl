#!/usr/bin/perl

use strict;
use warnings;
use Cwd;
use myutils;

my $file = shift;
my $reftable = shift;
my $i = shift;

# Obtain file data
my @data = get_file_data($file);

# Remove file extension
my @fname = split ( /\./, $file );
my $fileext = "." . pop(@fname);
$file =~ s/$fileext//g;

my $outfile = $file. "_renamed.txt";
open(OUT, ">$outfile");
	
if (defined $reftable & defined $i) {

	my $matrixhashref = refdata2hash($reftable, $i);
	my %matrixhash = %{$matrixhashref};

	foreach my $line (@data) {
		if ($line =~ m/^MOTIF/) {
			my @elements = split ( /\s+/, $line );
			my $newname = $elements[1] . "\t" . $matrixhash{$elements[1]};
			print(OUT "MOTIF $newname\n");
		}else {
			print(OUT $line);
		}
	}
	
}else {
	
	foreach my $line (@data) {
		if ($line =~ m/^MOTIF/) {
			my @elements = split ( /\s+/, $line );
			my $newname = $file . "_" . $elements[1];
			print(OUT "MOTIF $newname\n");
		}else {
			print(OUT $line);
		}
	}
	
}

close(OUT);

sub refdata2hash {
	# Store reference table information in a hash
	my ($fname, $idx) = @_;
	my %refhash = ();
	
	my @refdata = get_file_data($fname);
	my @idxlist = split ( ',', $idx);
	    
	foreach my $line (@refdata) {
		my @element = split ( '\t', $line );
		my $id = $element[0];
		my @annot;		
		for (my $j=0; $j<scalar(@idxlist); $j++) {
			push(@annot, $element[$idxlist[$j]-1]);	
		}
		$refhash{$id} = join('_', @annot);
	}
	return \%refhash;
}

exit;