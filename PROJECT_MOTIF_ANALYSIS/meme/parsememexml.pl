#!/usr/bin/perl

use strict;
use warnings;
use XML::Simple;

my $dir = $ARGV[0];
opendir(DIR, $dir) or die $!;
my $motiftype= $ARGV[1];
my %motifhash = ();

my $ethr = 0.01;

# Print header for meme output file
print "MEME version 4.4\n\n";
print "ALPHABET= ACGT\n\n";
print "strands: + -\n";
print "Background letter frequencies (from uniform background):\n";
print "A 0.25000 C 0.25000 G 0.25000 T 0.25000\n\n";

# Go through all files in $dir
while (my $file = readdir(DIR)) {

	# Ignore files beginning with a period
	next if ($file =~ m/^\./);
	# Check if $file is a file
	next unless (-f "$dir/$file");
	# Ignore non XML files
	next if ($file !~ m/.xml$/);
	
	getFreqMatrix($file, \%motifhash);
	
}

sub getFreqMatrix {
	
	my $filename = $_[0];
	
	my $xs1 = XML::Simple->new();
	my $doc = $xs1->XMLin($filename);

	# Remove file extension
	my @fname = split ( /\./, $filename );
	my $fileext = "." . pop(@fname);
	(my $motifid = $filename) =~ s/$fileext//g;

	my %mfile =  %{$doc->{motifs}};
	my $m = $mfile{'motif'};
	
	foreach my $motifhashref ( keys %$m ) {
		
		if ($m->{$motifhashref}->{'e_value'} <= $ethr) {

			(my $i = $m->{$motifhashref}->{'id'}) =~ s/motif_//;

			print "MOTIF ", $motifid, "_", $i, "\n\n";
			print "letter-probability matrix: alength= 4 w= ", $m->{$motifhashref}->{'width'}, " nsites= ", $m->{$motifhashref}->{'sites'}, " E= ", $m->{$motifhashref}->{'e_value'}, "\n";	
	
			my $amat = $m->{$motifhashref}->{'probabilities'}->{'alphabet_matrix'};
			
			foreach my $alphabetarrayref ( sort keys %$amat) {
				my $aref = $amat->{$alphabetarrayref};
				foreach my $alphabet (@$aref) {
					print "  ";
					foreach my $value ( sort keys %$alphabet ) {
						my $valueref = $alphabet->{$value};
						for (my $j=0; $j<=3; $j++) {
							my $contenthashref = @$valueref[$j];
							printf "%.6f\t", $contenthashref->{'content'};	
						}
					}
					print "\n";
				}
				print "\n";
			}
			print "\n";	
		} 
		
	}
	
}

exit;